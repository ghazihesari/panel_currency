@extends('layouts.app')

@section('content')
    <div class="container h-100" style="margin-top: 25px">
        <div class="row justify-content-center align-self-center h-100">
            <div class="col-md-6 align-self-center">
                <div class="card p-4">
                    <div class="text-center p-4">



                       {{-- <h3 class="mb-4 pb-4">فرم عضویت</h3>--}}
                        <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="m-portlet__body">
                                <div class="container p-0">
                                    <div class="row">
                                        <div class="col-12">
                <div class="card-header" style="font-size: 25px">{{ __('ورود به پنل کاربری') }}</div>

             {{--   <div class="card-body">--}}
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}" enctype="multipart/form-data">
                        @csrf



                        <div class="form-group row" style="padding-top: 38px">
                            <label for="email" class="col-sm-4 col-form-label ">{{ __('نام کاربری : ') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label ">{{ __('کلمه عبور : ') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('من را فراموش نکنید') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('ورود') }}
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('کلمه عبور خود را فراموش کردید ؟') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
