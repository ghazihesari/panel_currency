@extends('user.master')

@section('content')
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        لیست تیکت ها
                    </h3>
                </div>
            </div>
        </div>
    <div class="col-sm-9 col-sm-offset-3 col-md-12 col-md-offset-2 main" style="text-align: center">
        <div class="page-header head-section">

            <div class="btn-group">


            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>شناسه</th>
                    <th>موضوع</th>
                    <th>واحد پشتیبانی</th>
                    <th>اولویت</th>
                    <th>تاریخ</th>
                    <th>وضعیت</th>
                    <th>مدیریت</th>
                </tr>
                </thead>
                <tbody>
                @foreach($tickets1 as $ticket1)
                    <tr>
                        <td>{{ $ticket1->id }}</td>
                        <td>{{ $ticket1->title }}</td>

                        <td>
                            @if(  $ticket1->support_unit == 0)
                                {{ "سفارشات" }}
                            @elseif(  $ticket1->support_unit == 1)
                                    {{ "امور مالی" }}
                            @elseif(  $ticket1->support_unit == 2)
                                        {{ "امور فنی" }}
                            @elseif(  $ticket1->support_unit == 3)
                                            {{ "مدیریت" }}
                            @elseif(  $ticket1->support_unit == 4)
                                {{ "متفرقه" }}
                                @endif
                            </td>
                        <td>
                            @if(  $ticket1->priority == 0)
                                {{ "فوری" }}
                            @elseif(  $ticket1->priority == 1)
                                {{ "مهم" }}
                            @elseif(  $ticket1->priority == 2)
                                {{ "متوسط" }}
                            @elseif(  $ticket1->priority == 3)
                                {{ "معمولی" }}

                            @endif
                            </td>
                        <td>{{ $ticket1->created_at }}</td>
                        @if( $ticket1->state == 0 )
                            <td style="background-color: orange;"> بررسی نشده است </td>
                        @else
                            <td style="background-color: green;"> توسط مدیر بررسی شد </td>
                        @endif

                        <td>
                            <form action="{{ route('ticket.destroy'  , ['id' => $ticket1->id]) }}" method="post">
                                {{ method_field('delete') }}
                                {{ csrf_field() }}
                                <div class="btn-group btn-group-xs">
                                    <button type="submit" class="btn btn-danger">حذف</button>
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach




                </tbody>

            </table>
            <div style="text-align: center">
                {!! $tickets1->render() !!}
            </div>
        </div>

    </div>
    </div>
@endsection