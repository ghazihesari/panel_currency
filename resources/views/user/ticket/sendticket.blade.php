@extends('user.master')

@section('script')

@endsection

@section('content')

    <div class="col-md-12">

        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                        <h3 class="m-portlet__head-text">
                            درخواست پشتیبانی
                        </h3>
                    </div>
                </div>
            </div>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <div class="page-header head-section">

        </div>
        <form class="form-horizontal" action="{{ route('ticket.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('Admin.section.errors')

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="title" class="control-label" style="margin-top: 25px">موضوع درخواست</label>
                    <input type="text" class="form-control" name="title" id="title" placeholder="موضوع خود را وارد نمایید" value="{{ old('title') }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="support_unit" class="control-label">واحد پشتیبانی</label>
                    <select name="support_unit" id="support_unit" class="form-control">
                        <option value="0" selected>سفارشات</option>
                        <option value="1" selected>امور مالی</option>
                        <option value="2" selected>امور فنی</option>
                        <option value="3" selected>مدیریت</option>
                        <option value="4" selected>متفرقه</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="priority" class="control-label">اولویت</label>
                    <select name="priority" id="priority" class="form-control">
                        <option value="0" selected>فوری</option>
                        <option value="1" selected>مهم</option>
                        <option value="2" selected>متوسط</option>
                        <option value="3" selected>معمولی</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="description" class="control-label">توضیحات</label>
                    <input type="text" class="form-control" name="description" id="description" placeholder="توضیحات خود را وارد نمایید" value="{{ old('description') }}">
                </div>
            </div>

            <div class="form-group">

                <div class="col-lg-8">
                    <div class="fileupload fileupload-new" data-provides="fileupload">

                        <div>
                            <span class="btn btn-file btn-success"><span class="fileupload-new">انتخاب فایل</span>
                                <input type="file" name="imgticket" /></span>

                        </div>
                    </div>
                </div>
            </div>




            <div class="form-group">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-danger" style="margin: 20px">ارسال</button>
                </div>
            </div>
        </form>
    </div>
        </div>
    </div>
@endsection