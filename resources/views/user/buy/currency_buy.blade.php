@extends('user.master')

@section('script')
    <script>
        $('#currency_number').change(function(){
            $number = $(this).val();
            $fee = $('#currency_required :selected').data('purchaseprice');
            $('#total_price').html($number * $fee);
        });
    </script>
@endsection

@section('content')
    <div class="col-md-12">

        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                        <h3 class="m-portlet__head-text">
                            خرید ارز
                        </h3>
                    </div>
                </div>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="margin: 15px">
                <div class="page-header head-section">

                </div>
                <?php
                use App\status_buy_sale;
                $val=status_buy_sale::first();
                // dd($val->buy_paypal);
                ?>
                <form class="form-horizontal" action="{{ route('buy.store')}}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @include('Admin.section.errors')


                    <div class="form-group">
                        <div class="col-sm-12" >
                            <label for="currency_required" class="control-label">ارز مورد نیاز</label>

                            <select name="currency_required" id="currency_required" class="form-control">


                                @if($val->buy_dolar == 1)
                                    <option value="9" data-purchaseprice="{{ $price[0]->purchaseprice_dolar }}"> {{ "  قیمت خرید دلار=  " }}{{ $price[0]->purchaseprice_dolar }}{{--{{"  / تعداد موجودی =   "  }}   {{ $price[0]->count_dolar}}--}}</option>
                                @endif
                                @if($val->buy_yoro == 1)
                                    <option value="10" data-purchaseprice="{{ $price[0]->purchaseprice_yoro }}"> {{ "  قیمت خرید یورو =   " }}{{ $price[0]->purchaseprice_yoro }}{{--{{"  / تعداد موجودی =   "  }}   {{ $price[0]->count_yoro}}--}}</option>
                                @endif
                                @if($val->buy_lir == 1)
                                    <option value="11" data-purchaseprice="{{ $price[0]->purchaseprice_lir }}">{{ "  قیمت خرید لیر =   " }} {{ $price[0]->purchaseprice_lir }}{{--{{"  / تعداد موجودی =   "  }}   {{ $price[0]->count_lir}}--}}</option>
                                @endif
                                @if($val->buy_pond == 1)
                                    <option value="12" data-purchaseprice="{{ $price[0]->purchaseprice_pond }}">{{ "  قیمت خرید پوند =   " }} {{ $price[0]->purchaseprice_pond }}{{--{{"  / تعداد موجودی =   "  }}   {{ $price[0]->count_pond}}--}}</option>
                                @endif
                                @if($val->buy_rial == 1)
                                    <option value="13" data-purchaseprice="{{ $price[0]->purchaseprice_rial }}">{{ "  قیمت خرید ریال =   " }} {{ $price[0]->purchaseprice_rial }}{{--{{"  / تعداد موجودی =   "  }}   {{ $price[0]->count_lir}}--}}</option>
                                @endif
                                @if($val->buy_perfectmani == 1)
                                    <option value="0" data-purchaseprice="{{ $price[0]->purchaseprice_perfectmani }}"> {{ "  قیمت خرید پرفکت مانی =   " }}{{ $price[0]->purchaseprice_perfectmani }}{{--{{"  / تعداد موجودی =   "  }}   {{ $price[0]->count_perfectmani}}--}}</option>
                                @endif

                                @if($val->buy_webmani == 1)
                                    <option value="1" data-purchaseprice="{{ $price[0]->purchaseprice_webmani }}">{{ "  قیمت خرید وبمانی =  " }} {{ $price[0]->purchaseprice_webmani }}{{--{{"  / تعداد موجودی =   "  }}{{$price[0]->count_webmani}}--}}</option>
                                @endif
                                @if($val->buy_bitcoin == 1)
                                    <option value="2" data-purchaseprice="{{ $price[0]->purchaseprice_bitcoin }}"> {{ "  قیمت خرید بیت کوین(میلی) = " }}{{ $price[0]->purchaseprice_bitcoin }}{{--{{"  / تعداد موجودی =   "  }}{{$price[0]->count_bitcoin}}--}}</option>
                                @endif
                                @if($val->buy_paypal == 1)
                                    <option value="3" data-purchaseprice="{{ $price[0]->purchaseprice_paypal }}"> {{ " قیمت خرید پی پال = " }}{{ $price[0]->purchaseprice_paypal }}{{--{{"  / تعداد موجودی =   "  }}{{$price[0]->count_paypal}}--}}</option>
                                @endif
                                @if($val->buy_vax== 1)
                                    <option value="4" data-purchaseprice="{{ $price[0]->purchaseprice_vax }}">{{ "  قیمت خرید وکس = " }} {{ $price[0]->purchaseprice_vax }}{{--{{"  / تعداد موجودی =   "  }}{{$price[0]->count_vax}}--}}</option>
                                @endif
                                @if($val->buy_egopay == 1)
                                    <option value="5" data-purchaseprice="{{ $price[0]->purchaseprice_egopay }}">{{ "  قیمت خرید اگو پی = " }} {{ $price[0]->purchaseprice_egopay }}{{--{{"  / تعداد موجودی =   "  }}{{$price[0]->count_egopay}}--}}</option>
                                @endif
                                @if($val->buy_okpay == 1)
                                    <option value="6" data-purchaseprice="{{ $price[0]->purchaseprice_okpay }}">{{ "  قیمت خرید اوکی پی = " }} {{ $price[0]->purchaseprice_okpay }}{{--{{"  / تعداد موجودی =   "  }}{{$price[0]->count_okpay}}--}}</option>
                                @endif
                                @if($val->buy_payza == 1)
                                    <option value="7" data-purchaseprice="{{ $price[0]->purchaseprice_payza }}"> {{ "  قیمت خرید پی زا = " }}{{ $price[0]->purchaseprice_payza }}{{--{{"  / تعداد موجودی =   "  }}{{$price[0]->count_payza}}--}}</option>
                                @endif
                                @if($val->buy_estrippay == 1)
                                    <option value="8" data-purchaseprice="{{ $price[0]->purchaseprice_estrippay }}"> {{ "  قیمت خرید استریپ پی = " }}{{ $price[0]->purchaseprice_estrippay }}{{--{{"  / تعداد موجودی =   "  }}{{$price[0]->count_estrippay}}--}}</option>
                                @endif



                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="currency_number" class="control-label">تعداد ارز</label>
                            <input type="text" class="form-control" name="currency_number" id="currency_number" placeholder="تعداد خود را وارد نمایید" value="{{ old('currency_number') }}">
                        </div>
                    </div>





                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="" class="control-label">کارت بانکی</label>
                            <select name="card_number" id="card_number" class="form-control">
                                @foreach($cards as $card)

                                    <option value={{ $i++ }} selected> {{ $card-> card_number }} </option>

                                @endforeach
                            </select>
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="account_received" class="control-label">حساب دریافتی</label>
                            <input type="text" class="form-control" name="account_received" id="account_received" placeholder="حساب ، کیف پول با آدرس الکترونیک جهت دریافت ارز" value="{{ old('accountـreceived') }}">
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="col-lg-8">
                            <div class="fileupload fileupload-new" data-provides="fileupload">

                                <div>
                            <span class="btn btn-file btn-success"><span class="fileupload-new">انتخاب فایل</span>
                                <input type="file" name="imgbank" /></span>

                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="total_price " class="control-label"> قیمت کل  :
                                <span id="total_price">0</span>
                            </label>


                        <!--   <input type="text" class="form-control" name="total_price " id="total_price " placeholder="حساب ، کیف پول با آدرس الکترونیک جهت دریافت ارز" value="{{ old('accountـreceived') }}">
                -->
                        </div>

                    </div>






                    <div class="form-group">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-danger" style="margin-bottom: 20px">خرید و پرداخت</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection