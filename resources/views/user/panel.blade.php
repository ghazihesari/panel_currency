@extends('user.master')

@section('content')
<div class="m-portlet  m-portlet--unair">
    <div class="m-portlet__body  m-portlet__body--no-padding">
        <div class="row m-row--no-padding m-row--col-separator-xl">
            <div class="col-md-12 col-lg-6 col-xl-3">

                <div class="m-widget24">
                    <div class="m-widget24__item">
                        <h4 class="m-widget24__title">
                            <a href="\user\panel\buy" style="font-size: 21px">سفارشات خرید</a>
                        </h4><br>

                        <span class="m-widget24__stats m--font-brand">
                             {{ $countbuy }}
                        </span>
                        <div class="m--space-40"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-6 col-xl-3">

                <div class="m-widget24">
                    <div class="m-widget24__item">
                        <h4 class="m-widget24__title">
                            <a href="\user\panel\sale" style="font-size: 21px">سفارشات فروش</a>
                        </h4><br>
                       {{-- <span class="m-widget24__desc">
                            سفارشات سفارشات 
                        </span>--}}
                        <span class="m-widget24__stats m--font-info">
                           {{ $countsale }}
                        </span>
                        <div class="m--space-40"></div>
                    </div>
                </div>

            </div>
            <div class="col-md-12 col-lg-6 col-xl-3">

                <div class="m-widget24">
                    <div class="m-widget24__item">
                        <h4 class="m-widget24__title">
                            <a href="\user\panel\accountinformation" style="font-size: 21px">لیست کارت بانکی</a>
                        </h4><br>
                        {{--<span class="m-widget24__desc">
                            تراکنشات تراکنشات   
                        </span>--}}
                        <span class="m-widget24__stats m--font-danger">
                            {{ $countacc }}
                        </span>
                        <div class="m--space-40"></div>
                    </div>
                </div>

            </div>
            <div class="col-md-12 col-lg-6 col-xl-3">

                <!--begin::New Users-->
                <div class="m-widget24">
                    <div class="m-widget24__item">
                        <h4 class="m-widget24__title">
                            <a href="\user\panel\ticket" style="font-size: 21px">تیکت ها</a>
                        </h4><br>
                        {{--<span class="m-widget24__desc">
                            تیکت ها تیکت ها 
                        </span>--}}
                        <span class="m-widget24__stats m--font-success">
                            {{ $countticket }}
                        </span>
                        <div class="m--space-40"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">

        <div class="m-portlet m-portlet--full-height  m-portlet--unair">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            لیست سفارشات خرید
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">
                        <li class="nav-item m-tabs__item">
                            {{--<a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_widget11_tab1_content" role="tab">
                                ماه پیش
                            </a>--}}
                        </li>
                        <li class="nav-item m-tabs__item">
                            {{--<a class="nav-link m-tabs__link" data-toggle="tab" href="#m_widget11_tab2_content" role="tab">
                                سال اخیر
                            </a>--}}
                        </li>
                    </ul>
                </div>
            </div>

            <div class="col-sm-9 col-sm-offset-3 col-md-12 col-md-offset-2 main" style="text-align: center">
                <div class="page-header head-section">

                    <div class="btn-group">


                        <!--
                              comment
                        -->

                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>شناسه</th>
                            <th>نام و نام خانوادگی</th>
                            <th>تعداد ارز دریافتی</th>
                            <th>نوع ارز دریافتی</th>
                            <th>تاریخ</th>
                            <th>وضعیت</th>
                            <th>مدیریت</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($values as $value)
                            <tr>

                                <td>{{ $value->id }}</td>
                                @foreach($users as $user)
                                    <td>{{ $user->name }}</td>
                                @endforeach
                                <td>{{ $value->currency_number }}</td>

                                @if($value->currency_required  == 0)
                                    <td>پرفکت مانی</td>
                                @elseif($value->currency_required  == 1)
                                    <td>وبمانی</td>
                                @elseif($value->currency_required  == 2)
                                    <td>بیت کوین</td>
                                @elseif($value->currency_required  == 3)
                                    <td>پی پال</td>
                                @elseif($value->currency_required  == 4)
                                    <td>وکس</td>
                                @elseif($value->currency_required  == 5)
                                    <td>اگو پی</td>
                                @elseif($value->currency_required  == 6)
                                    <td>اوکی پی</td>
                                @elseif($value->currency_required  == 7)
                                    <td>پی زا</td>
                                @elseif($value->currency_required  == 8)
                                    <td>استریپ پی</td>
                                @elseif($value->currency_required  == 9)
                                    <td>دلار</td>
                                @elseif($value->currency_required  == 10)
                                    <td>یورو</td>
                                @elseif($value->currency_required  == 11)
                                    <td>لیر</td>
                                @elseif($value->currency_required  == 12)
                                    <td>پوند</td>
                                @elseif($value->currency_required  == 13)
                                    <td>ریال</td>

                                @endif
                                <td>{{ $value->created_at }}</td>

                                @if( $value->state == 0 )
                                    <td style="background-color: orange;"> بررسی نشده است </td>
                                @else
                                    <td style="background-color: green;"> توسط مدیر بررسی شد </td>
                                @endif

                                <td>
                                    <form action="{{ route('buy.destroy'  , ['id' => $value->id]) }}" method="post">
                                        {{ method_field('delete') }}
                                        {{ csrf_field() }}
                                        <div class="btn-group btn-group-xs">
                                            <button type="submit" class="btn btn-danger">حذف</button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach


                        </tbody>
                    </table>
                    <div style="text-align: center">
                        {!! $values->render() !!}
                    </div>
                </div>

            </div>
        </div>
    {{--    <div class="m-portlet m-portlet--full-height  m-portlet--unair">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            لیست سفارشات فروش
                        </h3>
                    </div>
                </div>

            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-12 col-md-offset-2 main" style="text-align: center">
                <div class="page-header head-section">

                    <div class="btn-group">


                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>شناسه</th>
                            <th>نام و نام خانوادگی</th>
                            <th>نوع ارز ارسالی</th>
                            <th>تاریخ</th>
                            <th>وضعیت</th>
                            <th>مدیریت</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($values as $value)
                            <tr>
                                <td>{{ $value->id }}</td>
                                @foreach($users as $user)
                                    <td>{{ $user->name }}</td>
                                @endforeach



                                @if($value->submitted_currency  == 0)
                                    <td>پرفکت مانی</td>
                                @elseif($value->submitted_currency  == 1)
                                    <td>وبمانی</td>
                                @elseif($value->submitted_currency  == 2)
                                    <td>بیت کوین</td>
                                @elseif($value->submitted_currency  == 3)
                                    <td>پی پال</td>
                                @elseif($value->submitted_currency  == 4)
                                    <td>وکس</td>
                                @elseif($value->submitted_currency  == 5)
                                    <td>اگو پی</td>
                                @elseif($value->submitted_currency  == 6)
                                    <td>اوکی پی</td>
                                @elseif($value->submitted_currency  == 7)
                                    <td>پی زا</td>
                                @elseif($value->submitted_currency  == 8)
                                    <td>استریپ پی</td>
                                @elseif($value->submitted_currency  == 9)
                                    <td>دلار</td>
                                @elseif($value->submitted_currency  == 10)
                                    <td>یورو</td>
                                @elseif($value->submitted_currency  == 11)
                                    <td>لیر</td>
                                @elseif($value->submitted_currency  == 12)
                                    <td>پوند</td>
                                @elseif($value->submitted_currency  == 13)
                                    <td>ریال</td>

                                @endif


                                <td>{{ $value->created_at }}</td>

                                @if( $value->state == 0 )
                                    <td style="background-color: orange;"> بررسی نشده است </td>
                                @else
                                    <td style="background-color: green;"> توسط مدیر بررسی شد </td>
                                @endif


                                <td>
                                    <form action="{{ route('sale.destroy'  , ['id' => $value->id]) }}" method="post">
                                        {{ method_field('delete') }}
                                        {{ csrf_field() }}
                                        <div class="btn-group btn-group-xs">
                                            <button type="submit" class="btn btn-danger">حذف</button>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach



                        </tbody>
                    </table>
                    <div style="text-align: center">
                        {!! $values->render() !!}
                    </div>
                </div>

            </div>
        </div>--}}

    </div>
</div>

@endsection