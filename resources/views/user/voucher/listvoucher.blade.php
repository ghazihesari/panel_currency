@extends('user.master')

@section('content')
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        لیست ووچرها
                    </h3>
                </div>
            </div>
        </div>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <div class="page-header head-section">
            <h2>

                </h2>
            <div class="btn-group">


            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>شناسه</th>
                    <th>شماره ووچر</th>
                    <th>کد ووچر</th>
                    <th>مبلغ ریالی</th>
                    <th>مبلغ ارزی</th>
                    <th>تاریخ</th>
                    <th>وضعیت</th>

                </tr>
                </thead>
                <tbody>



                </tbody>
            </table>
        </div>
        <div style="text-align: center">

        </div>
    </div>
    </div>
@endsection