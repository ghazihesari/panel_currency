@extends('user.master')

@section('script')

@endsection

@section('content')
    <div class="col-md-12">

        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                        <h3 class="m-portlet__head-text">
                            خرید ووچر پرفکت مانی
                        </h3>
                    </div>
                </div>
            </div>
    <div class="col-sm-12 col-sm-offset-3 col-md-6 col-md-offset-2 main" style="margin: 15px" >
        <div class="page-header head-section">

        </div>
        <form class="form-horizontal" action="{{ route('voucher.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}


            <div class="form-group">
                <div class="col-sm-12">
                    <label for="voucher_type" class="control-label">نوع ووچر</label>
                    <select name="voucher_type" id="voucher_type" class="form-control">
                        <option value="0" selected> 0</option>
                        <option value="1" selected>1</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="voucher_number" class="control-label">تعداد ارز</label>
                    <input type="text" class="form-control" name="voucher_number" id="voucher_number" placeholder="تعداد خود را وارد نمایید" value="{{ old('currency_number') }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="" class="control-label">کارت بانکی</label>
                    <select name="" id="" class="form-control">
                        @foreach($cards as $card)

                            <option value={{ $i++ }} selected> {{ $card-> card_number }} </option>

                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="accountـreceived" class="control-label">حساب دریافتی</label>
                    <input type="text" class="form-control" name="accountـreceived" id="accountـreceived" placeholder="حساب ، کیف پول با آدرس الکترونیک جهت دریافت ارز" value="{{ old('accountـreceived') }}">
                </div>
            </div>



            <div class="form-group">
                <div class="col-sm-12">
                    <label for="total_price " class="control-label"> : قیمت کل</label>
                <!--   <input type="text" class="form-control" name="total_price " id="total_price " placeholder="حساب ، کیف پول با آدرس الکترونیک جهت دریافت ارز" value="{{ old('accountـreceived') }}">
                -->
                </div>

            </div>






            <div class="form-group">
                <div class="col-sm-12" style="padding: 20px">
                    <button type="submit" class="btn btn-danger">خرید و پرداخت</button>
                </div>
            </div>
        </form>
    </div>
        </div>
    </div>
@endsection