@extends('user.master')

@section('script')

@endsection

@section('content')



    <div class="col-md-12">

        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                        <h3 class="m-portlet__head-text">
                            ارسال مدارک
                        </h3>
                    </div>
                </div>
            </div>

    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <div class="page-header head-section">

        </div>
        <form class="form-horizontal" action="{{ route('senddocuments.update' , ['id' => $value[0]->id]) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}


            <div class="col-md-8">
                <div class="form-group">
                    <label class="col-md-8 control-label"><i class="fa fa-user"></i> تصویر کاربر (3*4) :</label>
                    <div class="input-group input-group-sm">
                        <input name="imguser" class="" type="file" required>
                        <br>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-8 control-label"><i class="fa fa-file"></i> کارت ملی :</label>
                    <div class="input-group input-group-sm">
                        <input name="imgnationalcard" class="" type="file" required>
                        <br>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-8 control-label"><i class="fa fa-file"></i> اسکن شناسنامه :</label>
                    <div class="input-group input-group-sm">
                        <input name="imgidcard" class="" type="file" required
                        >
                        <br>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-8 control-label"><i class="fa fa-file"></i> اسکن قبض:</label>
                    <div class="input-group input-group-sm">
                        <input name="imgbill" class="" type="file" required
                        >
                        <br>
                    </div>
                </div>
                <br>
                <br>
                <div class="form-group">
                    <label class="control-label"><i class="fa fa-bullhorn"></i><b class="fsize14"> نکته :</b> </label>
                    یکی از قبوض برق , آب یا تلفن جهت احراز هویت کافی می باشد و همچنین در هنگام ارسال اسکن کارت ملی
                    باید کارت ملی را در دست خود نگه داشته و صورت و کارت ملی در یک راستا قابل مشاهده باشد . این عملیات
                    جهت احراز کارت ملی و تصویر الزامی می باشد.
                </div>

            </div>
            <br>










           {{-- <div class="form-group">

                <div class="col-lg-8">
                    <div class="fileupload fileupload-new" data-provides="fileupload">

                        <div>
                            <span class="btn btn-file btn-success"><span class="fileupload-new">تصویر کاربر</span>
                                <input type="file" name="imguser" /></span>

                        </div>
                    </div>
                </div>
            </div>



            <div class="form-group">

                <div class="col-lg-8">
                    <div class="fileupload fileupload-new" data-provides="fileupload">

                        <div>
                            <span class="btn btn-file btn-success"><span class="fileupload-new">کارت ملی</span>
                                <input type="file" name="imgnationalcard " /></span>

                        </div>
                    </div>
                </div>
            </div>


            <div class="form-group">

                <div class="col-lg-8">
                    <div class="fileupload fileupload-new" data-provides="fileupload">

                        <div>
                            <span class="btn btn-file btn-success"><span class="fileupload-new">اسکن شناسنامه</span>
                                <input type="file" name="imgidcard" /></span>

                        </div>
                    </div>
                </div>
            </div>


            <div class="form-group">

                <div class="col-lg-8">
                    <div class="fileupload fileupload-new" data-provides="fileupload">

                        <div>
                            <span class="btn btn-file btn-success"><span class="fileupload-new">اسکن قبض</span>
                                <input type="file" name="imgbill" /></span>

                        </div>
                    </div>
                </div>
            </div>
            <div>
                نکته : یکی از قبوض برق , آب یا تلفن جهت احراز هویت کافی می باشد و همچنین در هنگام ارسال اسکن کارت ملی باید کارت ملی را در دست خود نگه داشته و صورت و کارت ملی در یک راستا قابل مشاهده باشد . این عملیات جهت احراز کارت ملی و تصویر الزامی می باشد.
            </div>--}}



            <div class="form-group">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-danger" style="margin: 20px"> ارسال مدارک</button>
                </div>
            </div>
        </form>
    </div>
        </div>
    </div>

@endsection