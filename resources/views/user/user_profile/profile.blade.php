@extends('user.master')

@section('script')

@endsection

@section('content')
    <div class="col-md-12">

        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                        <h3 class="m-portlet__head-text">
                            پروفایل کاربری
                        </h3>
                    </div>
                </div>
            </div>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <div class="page-header head-section">

        </div>
        <form class="form-horizontal" action="{{ route('profile.update' , ['id' => $value[0]->id]) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="title" class="control-label">نام و نام خانوادگی</label>
                    <input type="text" class="form-control" name="name" id="name" placeholder="نام و نام خانوادگی خود را وارد نمایید" value="{{ $value[0]->name }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="sex" class="control-label">جنسیت</label>
                    <select name="sex" id="sex" class="form-control">
                        <option value="1" selected>مرد</option>
                        <option value="0" selected>زن</option>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="title" class="control-label">نام پدر</label>
                    <input type="text" class="form-control" name="father_name" id="father_name" placeholder="نام پدر خود را وارد کنید" value="{{ $value[0]->father_name }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="title" class="control-label">تاریخ تولد</label>
                    <input type="text" class="form-control" name="date_of_birth" id="date_of_birth" placeholder="تاریخ تولد خود را وارد کنید" value="{{ $value[0]->date_of_birth }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="title" class="control-label">شماره شناسنامه</label>
                    <input type="text" class="form-control" name="id_birth_certificate" id="id_birth_certificate" placeholder="شماره شناسنامه خود را وارد کنید" value="{{$value[0]->id_birth_certificate }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="title" class="control-label">کد ملی</label>
                    <input type="text" class="form-control" name="national_code" id="national_code" placeholder="کد ملی خود را وارد کنید" value="{{ $value[0]->national_code}}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="title" class="control-label">تلفن همراه</label>
                    <input type="text" class="form-control" name="mobile" id="mobile" placeholder="تلفن همراه خود را وارد کنید" value="{{ $value[0]->mobile }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="title" class="control-label">تلفن ثابت</label>
                    <input type="text" class="form-control" name="phone" id="phone" placeholder="تلفن ثابت خود را وارد کنید" value="{{ $value[0]->phone }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="title" class="control-label">استان</label>
                    <input type="text" class="form-control" name="province" id="province" placeholder="نام استان خود را وارد کنید" value="{{ $value[0]->province }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="title" class="control-label">شهر</label>
                    <input type="text" class="form-control" name="city" id="city" placeholder="نام شهر خود را وارد کنید" value="{{ $value[0]->city }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="title" class="control-label">آدرس</label>
                    <input type="text" class="form-control" name="address" id="address" placeholder="آدرس خود را وارد کنید" value="{{ $value[0]->address }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="title" class="control-label">کد پستی</label>
                    <input type="text" class="form-control" name="postal_code" id="postal_code" placeholder="کد پستی خود را وارد کنید" value="{{ $value[0]->postal_code }}">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="title" class="control-label">ایمیل</label>
                    <input type="text" class="form-control" name="email" id="email" placeholder="ایمیل خود را وارد کنید" value="{{ $value[0]->email }}">
                </div>
            </div>




            <div class="form-group">
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-danger" style="margin: 20px">بروزرسانی پروفایل</button>
                </div>
            </div>
        </form>
    </div>
        </div>
    </div>
@endsection