@extends('user.master')

@section('script')

@endsection

@section('content')

    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        پروفایل کاربری
                    </h3>
                </div>
            </div>
        </div>



        <form class="form-horizontal" action="{{ route('users.updatepass') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}


            <div class="form-group" style=" margin-top: 20px">
                <div class="col-sm-6">
                    <label for="pass1" class="control-label">کلمه ی عبور جدید:</label>
                    <input type="text" class="form-control" name="pass1" id="pass1" placeholder="" value="{{ old('name') }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6">
                    <label for="pass2" class="control-label">تکرار کلمه ی عبور:</label>
                    <input type="text" class="form-control" name="pass2" id="pass2" placeholder="" value="{{ old('name') }}">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12" style="margin-bottom: 20px">
                    <button type="submit" class="btn btn-danger" style="margin: 20px">بروزرسانی کلمه عبور</button>
                </div>
            </div>
        </form>


    </div>


@endsection