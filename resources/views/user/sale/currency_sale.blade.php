@extends('user.master')

@section('script')

@endsection

@section('content')
    <div class="col-md-12">

        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                        <h3 class="m-portlet__head-text">
                            فروش ارز
                        </h3>
                    </div>
                </div>
            </div>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <div class="page-header head-section">

        </div>
        <?php
        use App\status_buy_sale;
        $val=status_buy_sale::first();
        // dd($val->buy_paypal);
        ?>
        <form class="form-horizontal" action="{{ route('sale.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include('Admin.section.errors')


            <div class="form-group">
                <div class="col-sm-12" >
                    <label for="submitted_currency" class="control-label" style="margin-top: 25px">ارز ارسالی</label>

                    <select name="submitted_currency" id="submitted_currency" class="form-control">



                        @if($val->sale_dolar == 1)
                            <option value="9" selected> {{ "  قیمت فروش دلار = " }}{{ $price[0]->purchaseprice_dolar }}</option>
                        @endif
                        @if($val->sale_yoro == 1)
                            <option value="10" selected> {{ "  قیمت فروش یورو = " }}{{ $price[0]->purchaseprice_yoro }}</option>
                        @endif
                        @if($val->sale_lir == 1)
                            <option value="11" selected>{{ "  قیمت فروش لیر = " }} {{ $price[0]->purchaseprice_lir }}</option>
                        @endif
                        @if($val->sale_pond == 1)
                            <option value="12" selected> {{ "  قیمت فروش پوند = " }}{{ $price[0]->purchaseprice_pond }}</option>
                        @endif
                        @if($val->sale_rial == 1)
                            <option value="13" selected> {{ "  قیمت فروش ریال = " }}{{ $price[0]->purchaseprice_rial }}</option>
                        @endif
                        @if($val->sale_perfectmani == 1)
                            <option value="0" selected> {{ "  قیمت فروش پرفکت مانی = " }}{{ $price[0]->salesprice_perfectmani }}</option>
                        @endif

                        @if($val->sale_webmani == 1)
                            <option value="1" selected> {{ "  قیمت فروش وبمانی = " }}{{ $price[0]->salesprice_webmani }}</option>
                        @endif
                        @if($val->sale_bitcoin == 1)
                            <option value="2" selected> {{ "  قیمت فروش بیت کوین(میلی) = " }}{{ $price[0]->salesprice_bitcoin }}</option>
                        @endif
                        @if($val->sale_paypal == 1)
                            <option value="3" selected> {{ "  قیمت فروش پی پال = " }}{{ $price[0]->salesprice_paypal }}</option>
                        @endif
                        @if($val->sale_vax== 1)
                            <option value="4" selected> {{ "  قیمت فروش وکس = " }}{{ $price[0]->salesprice_vax }}</option>
                        @endif
                        @if($val->sale_egopay == 1)
                            <option value="5" selected>{{ "  قیمت فروش اگو پی = " }} {{ $price[0]->salesprice_egopay }}</option>
                        @endif
                        @if($val->sale_okpay == 1)
                            <option value="6" selected>{{ "  قیمت فروش اوکی پی = " }} {{ $price[0]->salesprice_okpay }}</option>
                        @endif
                        @if($val->sale_payza == 1)
                            <option value="7" selected>{{ "  قیمت فروش پی زا = " }} {{ $price[0]->salesprice_payza }}</option>
                        @endif
                        @if($val->sale_estrippay == 1)
                            <option value="8" selected> {{ "  قیمت فروش استریپ پی = " }}{{ $price[0]->salesprice_estrippay }}</option>
                        @endif





                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="price" class="control-label">مبلغ</label>
                    <input type="text" class="form-control" name="price" id="price" placeholder="قیمت خود را وارد نمایید" value="{{ old('price') }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="" class="control-label">بانک دریافتی</label>
                    <select name="" id="" class="form-control">
                        @foreach($values as $value){
                        <option value={{$i++}} selected> {{ $value->bankname }}</option>
                        }
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="currency_account" class="control-label"> حساب ارزی</label>
                    <input type="text" class="form-control" name="currency_account" id="currency_account" placeholder="آدرس کیف پول و حساب ارزی" value="{{ old('currency_account') }}">
                </div>
            </div>



           {{-- <div class="form-group">
                <div class="col-sm-12">
                    <label for="total_price " class="control-label">قیمت کل  :  </label>
                <!--   <input type="text" class="form-control" name="total_price " id="total_price " placeholder="حساب ، کیف پول با آدرس الکترونیک جهت دریافت ارز" value="{{ old('accountـreceived') }}">
                -->
                </div>

            </div>--}}






            <div class="form-group" >
                <div class="col-sm-12" >
                    <button type="submit" style="margin: 20px" class="btn btn-danger">فروش و پرداخت</button>
                </div>
            </div>
        </form>
    </div>
        </div>
    </div>
@endsection