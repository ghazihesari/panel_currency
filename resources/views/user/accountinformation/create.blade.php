@extends('user.master')

@section('script')

@endsection

@section('content')
    <div class="col-md-12">

        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                        <h3 class="m-portlet__head-text">
                            افزودن کارت بانکی
                        </h3>
                    </div>
                </div>
            </div>

    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="margin: 25px">
        <div class="page-header head-section">



        </div>
        <form class="form-horizontal" action="{{ route('accountinformation.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @include("Admin.section.errors")
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="account_number" class="control-label">شماره حساب</label>
                    <input type="text" class="form-control" name="account_number" id="account_number" placeholder="شماره حساب خود را وارد نمایید" value="{{ old('account_number') }}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="bank_name" class="control-label">نام بانک</label>
                    <select name="bank_name" id="bank_name" class="form-control">
                        @foreach($values as $value){
                        <option value={{$i++}} selected> {{ $value->bankname }}</option>
                            }
                            @endforeach

                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <label for="card_number" class="control-label">شماره کارت</label>
                    <input type="text" class="form-control" name="card_number" id="card_number" placeholder="شماره کارت خود را وارد نمایید" value="{{ old('card_number') }}">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-12">
                    <label for="shaba_number" class="control-label">شماره شبا</label>
                    <input type="text" class="form-control" name="shaba_number" id="shaba_number" placeholder="شماره شبا خود را وارد نمایید" value="{{ old('shaba_number') }}">
                </div>
            </div>




            <div class="form-group">

                <div class="col-lg-8">
                    <div class="fileupload fileupload-new" data-provides="fileupload">

                        <div>
                            <span class="btn btn-file btn-success"><span class="fileupload-new">اسکن کارت بانکی</span>
                                <input type="file" name="imgcard" /></span>

                        </div>
                    </div>
                </div>
            </div>





            <div class="form-group" >
                <div class="col-sm-12">
                    <button type="submit" class="btn btn-danger" style="margin: 20px">ارسال</button>
                </div>
            </div>
        </form>
    </div>
        </div>
    </div>
@endsection