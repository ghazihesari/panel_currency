@extends('Admin.master')

@section('content')
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        لیست کارت های بانکی
                    </h3>
                </div>
            </div>
        </div>
    <div class="col-sm-9 col-sm-offset-3 col-md-12 col-md-offset-2 main" style="text-align: center">
        <div class="page-header head-section">

            <div class="btn-group">


            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>شناسه</th>
                    <th>نام و نام خانوادگی</th>
                    <th>شماره حساب</th>
                    {{--<th>نام بانک</th>--}}
                    <th>شماره کارت</th>
                    <th>تاریخ</th>
                    <th>وضعیت</th>
                    <th>مدیریت</th>
                </tr>
                </thead>
                <tbody>
                @foreach($cards as $card)
                    <tr>
                        <td>{{ $card->id }}</td>
                        <td>
                            <?php
                            $name=\App\User::where('id',$card->user_id )->get();
                            // dd($name[0]);
                            ?>
                            {{ $name[0]->name }}
                        </td>
                        <td>{{ $card->account_number }}</td>
                       {{-- <td>
                            @if(  $card->bank_name == 0)
                                {{ "بانک آینده" }}
                            @elseif(  $card->bank_name == 1)
                                {{ "بانک ملی" }}
                            @elseif(  $card->bank_name == 2)
                                {{ "بانک سپه" }}
                            @elseif(  $card->bank_name == 3)
                                {{ "بانک پاسارگاد" }}
                            @elseif(  $card->bank_name == 4)
                                {{ "بانک صنعت و معدن" }}
                            @endif


                        </td>--}}
                        <td>{{ $card->card_number  }}</td>

                        <td>{{ $card->created_at }}</td>
                        <td>
                            @if(  $card->state == 0)
                                {{ " تایید نشده" }}
                            @else
                                {{ " تایید شد" }}
                                @endif

                                                    </td>

                        <td>
                            <form action="{{ route('delaccount'  , ['id' => $card->id]) }}" method="post">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}

                                <div class="btn-group btn-group-xs">
                                    <button type="submit" class="btn btn-danger">حذف</button>
                                </div>
                            </form>
                        </td>
                        <td>
                            <form action="{{ route('verifyaccount'  , ['id' => $card->id]) }}" method="post">

                                {{ csrf_field() }}
                                <div class="btn-group btn-group-xs">
                                    <button type="submit" class="btn btn-primary">تایید شود </button>
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach




                </tbody>
            </table>
            <div style="text-align: center">
                {!! $cards->render() !!}
            </div>
        </div>

    </div>
    </div>
@endsection