@extends('Admin.master')

@section('script')

@endsection

@section('content')
    <div class="col-md-12">

        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                        <h3 class="m-portlet__head-text">
                            مدیریت وضعیت خرید و فروش
                        </h3>
                    </div>
                </div>
            </div>


            <!--begin::Form-->
            <form class="form-horizontal" action="{{ route('statusbuysale.update', ['id'=>$status[0]->id]) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="m-portlet__body">
                    <div class="container p-0">
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sale_dolar" class="control-label">فروش دلار :</label>
                                    <select name="sale_dolar" id="sale_dolar" class="form-control">
                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>

                                    </select>
                                </div>
                                <div>

                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['sale_dolar'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>

                                </div>
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="buy_dolar" class="control-label">خرید دلار :</label>
                                    <select name="buy_dolar" id="buy_dolar" class="form-control">
                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>
                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['buy_dolar'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>



                                </div>
                                <hr>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sale_yoro" class="control-label">فروش یورو :</label>
                                    <select name="sale_yoro" id="sale_yoro" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>

                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['sale_yoro'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>

                                </div>
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="buy_yoro" class="control-label">خرید یورو:</label>
                                    <select name="buy_yoro" id="buy_yoro" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>
                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['buy_yoro'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>



                                </div>
                                <hr>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sale_lir" class="control-label">فروش لیر :</label>
                                    <select name="sale_lir" id="sale_lir" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>

                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['sale_lir'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>

                                </div>
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="buy_lir" class="control-label">خرید لیر :</label>
                                    <select name="buy_lir" id="buy_lir" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>
                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['buy_lir'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>



                                </div>
                                <hr>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sale_pond" class="control-label">فروش پوند :</label>
                                    <select name="sale_pond" id="sale_pond" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>

                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['sale_pond'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>

                                </div>
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="buy_pond" class="control-label">خرید پوند :</label>
                                    <select name="buy_pond" id="buy_pond" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>
                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['buy_pond'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>



                                </div>
                                <hr>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sale_rial" class="control-label">فروش ریال :</label>
                                    <select name="sale_rial" id="sale_rial" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>

                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['sale_rial'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>

                                </div>
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="buy_rial" class="control-label">خرید ریال :</label>
                                    <select name="buy_rial" id="buy_rial" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>
                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['buy_rial'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>



                                </div>
                                <hr>
                            </div>




                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sale_perfectmani" class="control-label">فروش پرفکت مانی به کاربر :</label>
                                    <select name="sale_perfectmani" id="sale_perfectmani" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>

                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['sale_perfectmani'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>

                                </div>
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="buy_perfectmani" class="control-label">خرید پرفکت مانی به کاربر :</label>
                                    <select name="buy_perfectmani" id="buy_perfectmani" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>
                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['buy_perfectmani'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>



                                </div>
                                <hr>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sale_webmani" class="control-label">فروش وبمانی به کاربر :</label>
                                    <select name="sale_webmani" id="sale_webmani" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>
                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['sale_webmani'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>



                                </div>
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="buy_webmani" class="control-label">خرید وبمانی به کاربر :</label>
                                    <select name="buy_webmani" id="buy_webmani" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>

                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['buy_webmani'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>

                                </div>
                                <hr>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sale_paypal" class="control-label">فروش پی پال به کاربر :</label>
                                    <select name="sale_paypal" id="sale_paypal" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>

                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['sale_paypal'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>

                                </div>
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="buy_paypal" class="control-label">خرید پی پال به کاربر :</label>
                                    <select name="buy_paypal" id="buy_paypal" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>

                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['buy_paypal'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>

                                </div>
                                <hr>
                            </div>



                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sale_bitcoin" class="control-label">فروش بیت کوین به کاربر :</label>
                                    <select name="sale_bitcoin" id="sale_bitcoin" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>

                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['sale_bitcoin'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>

                                </div>
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="buy_bitcoin" class="control-label">خرید بیت کویت به کاربر :</label>
                                    <select name="buy_bitcoin" id="buy_bitcoin" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>

                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['buy_bitcoin'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>

                                </div>
                                <hr>
                            </div>



                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sale_payza" class="control-label">فروش پی زا به کاربر :</label>
                                    <select name="sale_payza" id="sale_payza" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>

                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['sale_payza'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>

                                </div>
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="buy_payza" class="control-label">خرید پی زا به کاربر :</label>
                                    <select name="buy_payza" id="buy_payza" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>

                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['buy_payza'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>

                                </div>
                                <hr>
                            </div>




                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sale_okpay" class="control-label">فروش اوکی پی به کاربر :</label>
                                    <select name="sale_okpay" id="sale_okpay" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>

                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['sale_okpay'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>

                                </div>
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="buy_okpay" class="control-label">خرید اوکی پی به کاربر :</label>
                                    <select name="buy_okpay" id="buy_okpay" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>

                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['buy_okpay'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>

                                </div>
                                <hr>
                            </div>




                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sale_egopay" class="control-label">فروش اگو پی به کاربر :</label>
                                    <select name="sale_egopay" id="sale_egopay" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>

                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['sale_egopay'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>

                                </div>
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="buy_egopay" class="control-label">خرید اگو پی به کاربر :</label>
                                    <select name="buy_egopay" id="buy_egopay" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>

                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['buy_egopay'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>

                                </div>
                                <hr>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sale_estrippay" class="control-label">فروش استریپ پی به کاربر :</label>
                                    <select name="sale_estrippay" id="sale_estrippay" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>

                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['sale_estrippay'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>

                                </div>
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="buy_estrippay" class="control-label">خرید استریپ پی به کاربر :</label>
                                    <select name="buy_estrippay" id="buy_estrippay" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>

                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['buy_estrippay'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>

                                </div>
                                <hr>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sale_vax" class="control-label">فروش وکس به کاربر :</label>
                                    <select name="sale_vax" id="sale_vax" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>

                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['sale_vax'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>

                                </div>
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="buy_vax" class="control-label">خرید وکس به کاربر :</label>
                                    <select name="buy_vax" id="buy_vax" class="form-control">

                                        <option value="0" selected> غیر فعال</option>
                                        <option value="1" selected> فعال</option>
                                    </select>
                                </div>
                                <div>

                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if($status[0]['buy_vax'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>

                                </div>
                                <hr>
                            </div>












                        </div>
                    </div>
                </div>







                <div class="m-portlet__foot">
                    <div class="m-form__actions">
                        <button type="submit" class="btn btn-primary">ویرایش وضعیت ها</button>

                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

        <!--end::Portlet-->


    </div>

@endsection