@extends('Admin.master')

@section('script')

@endsection

@section('content')
    <div class="page-content-wrap">

    <div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                <h3 class="m-portlet__head-text">
                    تنظیمات اطلاع رسانی
                </h3>
            </div>
        </div>
    </div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                <!-- Start Section Two--->
                <form action="http://www.a1z.ir/DemoExChanger/@moderator/notifications" method="POST"  role="form" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="92tlePEudM2XUja50mk8YlAx7PRPvrSUsLxcjhWE">
                    <div class="col-md-7">

                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال ایمیل بعد عضویت به کاربر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="email_resgiter_user" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                            <div class="form-groups">
                                <label class="col-md-6 control-label mgtop8">وضعیت فعلی : غیرفعال

                                </label>
                                <div class="input-group input-group-sm">

                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال ایمیل بعد از تایید مدارک به کاربر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="email_document_user" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال ایمیل بعد از تایید کاربر به کاربر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="email_verify_user" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال ایمیل بعد از تایید حساب بانکی به کاربر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="email_irbank_user" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال ایمیل بعد از تراکنش به کاربر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="email_transaction_user" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال ایمیل بعد از تایید سفارش به کاربر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="email_invoice_user" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال ایمیل بعد از تغییر وضعیت تیکت به کاربر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="email_ticket_user" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال ایمیل بعد عضویت به مدیر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="email_resgiter_admin" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال ایمیل بعد از تایید مدارک به مدیر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="email_document_admin" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال ایمیل بعد از تایید کاربر به مدیر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="email_verify_admin" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال ایمیل بعد از تایید حساب بانکی به مدیر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="email_irbank_admin" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال ایمیل بعد از تراکنش به مدیر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="email_transaction_admin" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال ایمیل بعد از تایید سفارش به مدیر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="email_invoice_admin" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال ایمیل بعد از تغییر وضعیت تیکت به مدیر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="email_ticket_admin" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال پیامک بعد عضویت به کاربر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="sms_resgiter_user" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال پیامک بعد از تایید مدارک به کاربر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="sms_document_user" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال پیامک بعد از تایید کاربر به کاربر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="sms_verify_user" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال پیامک بعد از تایید حساب بانکی به کاربر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="sms_irbank_user" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال پیامک بعد از تراکنش به کاربر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="sms_transaction_user" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال پیامک بعد از تایید سفارش به کاربر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="sms_invoice_user" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال پیامک بعد از تغییر وضعیت تیکت به کاربر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="sms_ticket_user" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال پیامک بعد عضویت به مدیر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="sms_resgiter_admin" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال پیامک بعد از تایید مدارک به مدیر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="sms_document_admin" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال پیامک بعد از تایید کاربر به مدیر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="sms_verify_admin" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال پیامک بعد از تایید حساب بانکی به مدیر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="sms_irbank_admin" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال پیامک بعد از تراکنش به مدیر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="sms_transaction_admin" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال پیامک بعد از تایید سفارش به مدیر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="sms_invoice_admin" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-6 control-label mgtop8">ارسال پیامک بعد از تغییر وضعیت تیکت به مدیر :</label>
                            <div class="input-group input-group-sm">
                                <span class="input-group-addon"><i class="fa fa-mobile"></i></span>
                                <select class="form-control" name="sms_ticket_admin" >
                                    <option value="1">غیرفعال</option>
                                    <option value="2">فعال</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-danger">ویرایش تنظیمات اطلاع رسانی ها</button>

                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="form-groups">
                            <label class="col-md-6 control-label mgtop8">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-danger">غیر فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-success">فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-success">فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-success">فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-success">فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-success">فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-success">فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-success">فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-success">فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-success">فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-success">فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-success">فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-danger">غیر فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-success">فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-danger">غیر فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-danger">غیر فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-danger">غیر فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-danger">غیر فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-danger">غیر فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-danger">غیر فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-danger">غیر فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-success">فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-danger">غیر فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-danger">غیر فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-danger">غیر فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-success">فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-danger">غیر فعال</a> </p>
                            </div>
                        </div>
                        <div class="form-groups">
                            <label class="col-md-6 control-label">وضعیت فعلی :</label>
                            <div class="input-group input-group-sm">
                                <p> <a class="btn btn-success">فعال</a> </p>
                            </div>
                        </div>

                    </div>

                </form>
            </div>
        </div>
    </div>
</div>


@endsection