@extends('Admin.master')

@section('script')

@endsection

@section('content')
    <div class="col-md-12">

        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                        <h3 class="m-portlet__head-text">
                            تنظیمات اطلاع رسانی
                        </h3>
                    </div>
                </div>
            </div>


            <!--begin::Form-->
            <form class="form-horizontal" action="{{ route('notifsetting.update' , ['id'=>$status[0]->id]) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="m-portlet__body">
                    <div class="container p-0">
                        <div class="row">



                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendemail_join_the_user" class="control-label">ارسال ایمیل بعد عضویت به کاربر :</label>
                                    <div class="input-group input-group-sm">
                                    <select name="sendemail_join_the_user" id="sendemail_join_the_user" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                    </div>
                                </div>
                                <div>

                                    <p class="mgtop8" style="color: darkred"><b>وضعیت فعلی :
                                            @if( 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>

                                </div>
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendemail_verification_of_douc_the_user" class="control-label">ارسال ایمیل بعد از تایید مدارک به کاربر :</label>
                                    <div class="input-group input-group-sm">
                                    <select name="sendemail_verification_of_douc_the_user" id="sendemail_verification_of_douc_the_user" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                    </div>
                                </div>
                                <div>
                                    <p class="mgtop8"><b>وضعیت فعلی :
                                            @if($status[0]['sendemail_verification_of_douc_the_user'] == 1)

                                                {{ "فعال" }}

                                            @else
                                                {{ "غیر فعال" }}

                                            @endif
                                        </b> </p>



                                </div>
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendemail_user_confirmation_user" class="control-label">ارسال ایمیل بعد از تایید کاربر به کاربر :</label>
                                    <select name="sendemail_user_confirmation_user" id="sendemail_user_confirmation_user" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendemail_verify_bankaccount_user" class="control-label">ارسال ایمیل بعد از تایید حساب بانکی به کاربر :</label>
                                    <select name="sendemail_verify_bankaccount_user" id="sendemail_verify_bankaccount_user" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendemail_transaction_user" class="control-label">ارسال ایمیل بعد از تراکنش به کاربر :</label>
                                    <select name="sendemail_transaction_user" id="sendemail_transaction_user" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendemail_order_confirmation_user" class="control-label">ارسال ایمیل بعد از تایید سفارش به کاربر :</label>
                                    <select name="sendemail_order_confirmation_user" id="sendemail_order_confirmation_user" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendemail_change_tickerstatus_user" class="control-label">ارسال ایمیل بعد از تغییر وضعیت تیکت به کاربر :</label>
                                    <select name="sendemail_change_tickerstatus_user" id="sendemail_change_tickerstatus_user" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendemail_subscribe_admin" class="control-label">ارسال ایمیل بعد عضویت به مدیر :</label>
                                    <select name="sendemail_subscribe_admin" id="sendemail_subscribe_admin" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendemail_verification_documents_manager" class="control-label">ارسال ایمیل بعد از تایید مدارک به مدیر :</label>
                                    <select name="sendemail_verification_documents_manager" id="sendemail_verification_documents_manager" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendemail_user_confirmation_admin" class="control-label">ارسال ایمیل بعد از تایید کاربر به مدیر :</label>
                                    <select name="sendemail_user_confirmation_admin" id="sendemail_user_confirmation_admin" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendemail_confirming_bank_account_manager" class="control-label">ارسال ایمیل بعد از تایید حساب بانکی به مدیر :</label>
                                    <select name="sendemail_confirming_bank_account_manager" id="sendemail_confirming_bank_account_manager" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendemail_transaction_manager" class="control-label">ارسال ایمیل بعد از تراکنش به مدیر :</label>
                                    <select name="sendemail_transaction_manager" id="sendemail_transaction_manager" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendemail_order_confirmation_manager" class="control-label">ارسال ایمیل بعد از تایید سفارش به مدیر :</label>
                                    <select name="sendemail_order_confirmation_manager" id="sendemail_order_confirmation_manager" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendmail_change_tickerstatus_manager" class="control-label">ارسال ایمیل بعد از تغییر وضعیت تیکت به مدیر :</label>
                                    <select name="sendmail_change_tickerstatus_manager" id="sendmail_change_tickerstatus_manager" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendsms_join_the_user" class="control-label">ارسال پیامک بعد عضویت به کاربر :</label>
                                    <select name="sendsms_join_the_user" id="sendsms_join_the_user" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendsms_verification_of_douc_the_user" class="control-label">ارسال پیامک بعد از تایید مدارک به کاربر :</label>
                                    <select name="sendsms_verification_of_douc_the_user" id="sendsms_verification_of_douc_the_user" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendsms_user_confirmation_user" class="control-label">ارسال پیامک بعد از تایید کاربر به کاربر :</label>
                                    <select name="sendsms_user_confirmation_user" id="sendsms_user_confirmation_user" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendsms_verify_bankaccount_user" class="control-label">ارسال پیامک بعد از تایید حساب بانکی به کاربر :</label>
                                    <select name="sendsms_verify_bankaccount_user" id="sendsms_verify_bankaccount_user" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendsms_transaction_user" class="control-label">ارسال پیامک بعد از تراکنش به کاربر :</label>
                                    <select name="sendsms_transaction_user" id="sendsms_transaction_user" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendsms_order_confirmation_user" class="control-label">ارسال پیامک بعد از تایید سفارش به کاربر :</label>
                                    <select name="sendsms_order_confirmation_user" id="sendsms_order_confirmation_user" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendsms_change_tickerstatus_user" class="control-label">ارسال پیامک بعد از تغییر وضعیت تیکت به کاربر :</label>
                                    <select name="sendsms_change_tickerstatus_user" id="sendsms_change_tickerstatus_user" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendsms_subscribe_admin" class="control-label">ارسال پیامک بعد عضویت به مدیر :</label>
                                    <select name="sendsms_subscribe_admin" id="sendsms_subscribe_admin" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendsms_verification_documents_manager" class="control-label">ارسال پیامک بعد از تایید مدارک به مدیر :</label>
                                    <select name="sendsms_verification_documents_manager" id="sendsms_verification_documents_manager" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendsms_user_confirmation_admin" class="control-label">ارسال پیامک بعد از تایید کاربر به مدیر :</label>
                                    <select name="sendsms_user_confirmation_admin" id="sendsms_user_confirmation_admin" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendsms_confirming_bank_account_manager" class="control-label">ارسال پیامک بعد از تایید حساب بانکی به مدیر :</label>
                                    <select name="sendsms_confirming_bank_account_manager" id="sendsms_confirming_bank_account_manager" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendsms_transaction_manager" class="control-label">ارسال پیامک بعد از تراکنش به مدیر :</label>
                                    <select name="sendsms_transaction_manager" id="sendsms_transaction_manager" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendsms_order_confirmation_manager" class="control-label">ارسال پیامک بعد از تایید سفارش به مدیر :</label>
                                    <select name="sendsms_order_confirmation_manager" id="sendsms_order_confirmation_manager" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sendsms_change_tickerstatus_manager" class="control-label">ارسال پیامک بعد از تغییر وضعیت تیکت به مدیر :</label>
                                    <select name="sendsms_change_tickerstatus_manager" id="sendsms_change_tickerstatus_manager" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>












                        </div>
                    </div>
                </div>







                <div class="m-portlet__foot">
                    <div class="m-form__actions">
                        <button type="submit" class="btn btn-primary">ویرایش تنظیمات اطلاع رسانی</button>

                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

        <!--end::Portlet-->


    </div>

@endsection