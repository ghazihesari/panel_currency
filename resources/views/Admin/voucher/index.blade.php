@extends('Admin.master')

@section('content')
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        سفارشات ووچر
                    </h3>
                </div>
            </div>
        </div>
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <div class="page-header head-section">

            <div class="btn-group">


                <!--
                      comment
                -->

            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>شناسه</th>
                    <th>خریدار</th>
                    <th>شماره ووچر</th>
                    <th>کد ووچر</th>
                    <th>مبلغ ریالی</th>
                    <th>مبلغ ارزی</th>
                    <th>تاریخ</th>
                    <th>وضعیت</th>
                    <th>مدیریت</th>
                </tr>
                </thead>
                <tbody>
                @foreach($values as $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                             @foreach($users as $user)
                                <td>{{ $user->name }}</td>
                             @endforeach

                                <td>{{ $value->voucher_number }}</td>
                                <td>{{ $value->voucher_code }}</td>
                                <td>{{ $value->price_rial }}</td>
                                <td>{{ $value->price_arz }}</td>
                                <td>{{ $value->created_at }}</td>
                                <td>{{ $value->status }}</td>

                        <td>
                            <form action="{{ route('buy.destroy'  , ['id' => $value->id]) }}" method="post">
                                {{ method_field('delete') }}
                                {{ csrf_field() }}
                                <div class="btn-group btn-group-xs">
                                    <button type="submit" class="btn btn-danger">حذف</button>
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach


                </tbody>
            </table>
        </div>
        <div style="text-align: center">

        </div>
    </div>
    </div>
@endsection