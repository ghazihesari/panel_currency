@extends('Admin.master')

@section('script')

@endsection

@section('content')
    <div class="col-md-12">

        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                        <h3 class="m-portlet__head-text">
                            مدیریت نرخ ارزها
                        </h3>
                    </div>
                </div>
            </div>

            <!--begin::Form-->

            <?php
               // dd($managingCurrency[0])->toArray();
            ?>
            <form class="form-horizontal" action="{{  route('managingcurrency.update' , ['id'=>$managingCurrency[0]->id]) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="m-portlet__body">
                    <div class="container p-0">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="purchaseprice_dolar" class="control-label">قیمت خرید دلار</label>
                                    <input type="text" class="form-control" name="purchaseprice_dolar" id="purchaseprice_dolar" placeholder="قیمت خرید دلار" value="{{ $managingCurrency[0]->purchaseprice_dolar}}">
                                </div>

                            </div>
                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="salesprice_dolar" class="control-label">قیمت فروش دلار</label>
                                    <input type="text" class="form-control" name="salesprice_dolar" id="salesprice_dolar" placeholder="قیمت فروش دلار" value="{{$managingCurrency[0]->salesprice_dolar}}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group m-form__group">
                                    <label for="count_dolar" class="control-label"> تعداد دلار موجود</label>
                                    <input type="text" class="form-control" name="count_dolar" id="count_dolar" placeholder=" تعداد دلار موجود" value="{{$managingCurrency[0]->count_dolar}}">
                                </div>
                            </div>
                            <div class="col-md-5">
                            <div class="form-group m-form__group">
                                <label for="purchaseprice_yoro" class="control-label">قیمت خرید یورو</label>
                                <input type="text" class="form-control" name="purchaseprice_yoro" id="purchaseprice_yoro" placeholder="قیمت خرید یورو" value="{{$managingCurrency[0]->purchaseprice_yoro}}">
                            </div>

                        </div>
                        <div class="col-md-5">
                            <div class="form-group m-form__group">
                                <label for="salesprice_yoro" class="control-label">قیمت فروش یورو </label>
                                <input type="text" class="form-control" name="salesprice_yoro" id="salesprice_yoro" placeholder="قیمت فروش یورو" value="{{$managingCurrency[0]->salesprice_yoro}}">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group m-form__group">
                                <label for="count_yoro" class="control-label"> تعداد یورو  موجود</label>
                                <input type="text" class="form-control" name="count_yoro" id="count_yoro" placeholder="تعداد یورو  موجود" value="{{$managingCurrency[0]->count_yoro}}">
                            </div>
                        </div>
                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="purchaseprice_lir" class="control-label">قیمت خرید لیر</label>
                                    <input type="text" class="form-control" name="purchaseprice_lir" id="purchaseprice_lir" placeholder="قیمت خرید لیر" value="{{$managingCurrency[0]->purchaseprice_lir}}">
                                </div>

                            </div>
                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="salesprice_lir" class="control-label">قیمت فروش لیر </label>
                                    <input type="text" class="form-control" name="salesprice_lir" id="salesprice_lir" placeholder="قیمت فروش لیر" value="{{$managingCurrency[0]->salesprice_lir}}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group m-form__group">
                                    <label for="count_lir" class="control-label"> تعداد لیر موجود</label>
                                    <input type="text" class="form-control" name="count_lir" id="count_lir" placeholder="تعداد لیر موجود" value="{{$managingCurrency[0]->count_lir}}">
                                </div>
                            </div>

                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="purchaseprice_pond" class="control-label">قیمت خرید پوند</label>
                                    <input type="text" class="form-control" name="purchaseprice_pond" id="purchaseprice_pond" placeholder="قیمت خرید پوند" value="{{$managingCurrency[0]->purchaseprice_pond}}">
                                </div>

                            </div>
                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="salesprice_pond" class="control-label">قیمت فروش پوند</label>
                                    <input type="text" class="form-control" name="salesprice_pond" id="salesprice_pond" placeholder="قیمت فروش پوند" value="{{$managingCurrency[0]->salesprice_pond}}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group m-form__group">
                                    <label for="count_pond" class="control-label"> تعداد پوند موجود</label>
                                    <input type="text" class="form-control" name="count_pond" id="count_pond" placeholder=" تعداد پوند موجود" value="{{$managingCurrency[0]->count_pond}}">
                                </div>
                            </div>

                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="purchaseprice_rial" class="control-label">قیمت خرید ریال</label>
                                    <input type="text" class="form-control" name="purchaseprice_rial" id="purchaseprice_rial" placeholder="قیمت خرید ریال" value="{{$managingCurrency[0]->purchaseprice_rial}}">
                                </div>

                            </div>
                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="salesprice_rial" class="control-label">قیمت فروش ریال</label>
                                    <input type="text" class="form-control" name="salesprice_rial" id="salesprice_rial" placeholder="قیمت فروش ریال" value="{{$managingCurrency[0]->salesprice_rial}}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group m-form__group">
                                    <label for="count_rial" class="control-label"> تعداد ریال موجود</label>
                                    <input type="text" class="form-control" name="count_rial" id="count_rial" placeholder="تعداد خرید ریال" value="{{$managingCurrency[0]->count_rial}}">
                                </div>
                            </div>




                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="purchaseprice_perfectmani" class="control-label">قیمت خرید پرفکت مانی</label>
                                    <input type="text" class="form-control" name="purchaseprice_perfectmani" id="purchaseprice_perfectmani" placeholder="قیمت خرید پرفکت مانی" value="{{$managingCurrency[0]->purchaseprice_perfectmani}}">
                                </div>

                            </div>
                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="salesprice_perfectmani" class="control-label">قیمت فروش پرفکت مانی</label>
                                    <input type="text" class="form-control" name="salesprice_perfectmani" id="salesprice_perfectmani" placeholder="قیمت فروش پرفکت مانی" value="{{$managingCurrency[0]->salesprice_perfectmani}}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group m-form__group">
                                    <label for="count_perfectmani" class="control-label"> تعداد پرفکت مانی موجود</label>
                                    <input type="text" class="form-control" name="count_perfectmani" id="count_perfectmani" placeholder="تعداد پرفکت مانی" value="{{$managingCurrency[0]->count_perfectmani}}">
                                </div>
                            </div>

                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="purchaseprice_webmani" class="control-label">قیمت خرید وبمانی</label>
                                    <input type="text" class="form-control" name="purchaseprice_webmani" id="purchaseprice_webmani" placeholder="قیمت خرید وبمانی" value="{{$managingCurrency[0]->purchaseprice_webmani}}">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="salesprice_webmani" class="control-label">قیمت فروش وبمانی</label>
                                    <input type="text" class="form-control" name="salesprice_webmani" id="salesprice_webmani" placeholder="قیمت فروش وبمانی" value="{{$managingCurrency[0]->salesprice_webmani}}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group m-form__group">
                                    <label for="count_webmani" class="control-label"> تعداد وبمانی موجود</label>
                                    <input type="text" class="form-control" name="count_webmani" id="count_webmani" placeholder="تعداد وبمانی" value="{{$managingCurrency[0]->count_webmani}}">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="purchaseprice_bitcoin" class="control-label">قیمت خرید بیت کوین (میلی)</label>
                                    <input type="text" class="form-control" name="purchaseprice_bitcoin" id="purchaseprice_bitcoin" placeholder="قیمت خرید بیت کوین (میلی)" value="{{$managingCurrency[0]->purchaseprice_bitcoin}}">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="salesprice_bitcoin" class="control-label">قیمت فروش بیت کوین (میلی)</label>
                                    <input type="text" class="form-control" name="salesprice_bitcoin" id="salesprice_bitcoin" placeholder="قیمت فروش بیت کوین (میلی)" value="{{$managingCurrency[0]->salesprice_bitcoin}}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group m-form__group">
                                    <label for="count_bitcoin" class="control-label">تعداد بیت کوین موجود</label>
                                    <input type="text" class="form-control" name="count_bitcoin" id="count_bitcoin" placeholder="تعداد بیت کوین (میلی)" value="{{$managingCurrency[0]->count_bitcoin}}">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="purchaseprice_paypal" class="control-label">قیمت خرید پی پال</label>
                                    <input type="text" class="form-control" name="purchaseprice_paypal" id="purchaseprice_paypal" placeholder="قیمت خرید پی پال" value="{{$managingCurrency[0]->purchaseprice_paypal}}">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="salesprice_paypal" class="control-label">قیمت فروش پی پال</label>
                                    <input type="text" class="form-control" name="salesprice_paypal" id="salesprice_paypal" placeholder="قیمت فروش پی پال" value="{{$managingCurrency[0]->salesprice_paypal}}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group m-form__group">
                                    <label for="count_paypal" class="control-label">تعداد پی پال موجود</label>
                                    <input type="text" class="form-control" name="count_paypal" id="count_paypal" placeholder="تعداد پی پال" value="{{$managingCurrency[0]->count_paypal}}">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="purchaseprice_vax" class="control-label">قیمت خرید وکس</label>
                                    <input type="text" class="form-control" name="purchaseprice_vax" id="purchaseprice_vax" placeholder="قیمت خرید وکس" value="{{$managingCurrency[0]->purchaseprice_vax}}">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="salesprice_vax" class="control-label">قیمت فروش وکس</label>
                                    <input type="text" class="form-control" name="salesprice_vax" id="salesprice_vax" placeholder="قیمت فروش وکس" value="{{$managingCurrency[0]->salesprice_vax}}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group m-form__group">
                                    <label for="count_vax" class="control-label">تعداد وکس موجود</label>
                                    <input type="text" class="form-control" name="count_vax" id="count_vax" placeholder="تعداد وکس" value="{{$managingCurrency[0]->count_vax}}">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="purchaseprice_egopay" class="control-label">قیمت خرید اگو پی</label>
                                    <input type="text" class="form-control" name="purchaseprice_egopay" id="purchaseprice_egopay" placeholder="قیمت خرید اگو پی" value="{{$managingCurrency[0]->purchaseprice_egopay}}">
                                </div>
                            </div><div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="salesprice_egopay" class="control-label">قیمت فروش اگو پی</label>
                                    <input type="text" class="form-control" name="salesprice_egopay" id="salesprice_egopay" placeholder="قیمت فروش اگو پی" value="{{$managingCurrency[0]->salesprice_egopay}}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group m-form__group">
                                    <label for="count_egopay" class="control-label">تعداد اگو پی موجود</label>
                                    <input type="text" class="form-control" name="count_egopay" id="count_egopay" placeholder="تعداد اگو پی" value="{{$managingCurrency[0]->count_egopay}}">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="purchaseprice_okpay" class="control-label">قیمت خرید اوکی پی</label>
                                    <input type="text" class="form-control" name="purchaseprice_okpay" id="purchaseprice_okpay" placeholder="قیمت خرید اوکی پی" value="{{$managingCurrency[0]->purchaseprice_okpay}}">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="salesprice_okpay" class="control-label">قیمت فروش اوکی پی</label>
                                    <input type="text" class="form-control" name="salesprice_okpay" id="salesprice_okpay" placeholder="قیمت فروش اوکی پی" value="{{$managingCurrency[0]->salesprice_okpay}}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group m-form__group">
                                    <label for="count_okpay" class="control-label">تعداد اوکی پی موجود</label>
                                    <input type="text" class="form-control" name="count_okpay" id="count_okpay" placeholder="تعداد اوکی پی" value="{{$managingCurrency[0]->count_okpay}}">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="purchaseprice_payza" class="control-label">قیمت خرید پی زا</label>
                                    <input type="text" class="form-control" name="purchaseprice_payza" id="purchaseprice_payza" placeholder="قیمت خرید پی زا" value="{{$managingCurrency[0]->purchaseprice_payza}}">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="salesprice_payza" class="control-label">قیمت فروش پی زا</label>
                                    <input type="text" class="form-control" name="salesprice_payza" id="salesprice_payza" placeholder="قیمت فروش پی زا" value="{{$managingCurrency[0]->salesprice_payza}}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group m-form__group">
                                    <label for="count_payza" class="control-label">تعداد پی زا موجود</label>
                                    <input type="text" class="form-control" name="count_payza" id="count_payza" placeholder="تعداد پی زا" value="{{$managingCurrency[0]->count_payza}}">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="purchaseprice_estrippay" class="control-label">قیمت خرید استریپ پی</label>
                                    <input type="text" class="form-control" name="purchaseprice_estrippay" id="purchaseprice_estrippay" placeholder="قیمت خرید استریپ پی" value="{{$managingCurrency[0]->purchaseprice_estrippay}}">
                                </div>
                            </div>

                            <div class="col-md-5">
                                <div class="form-group m-form__group">
                                    <label for="salesprice_estrippay" class="control-label">قیمت فروش استریپ پی</label>
                                    <input type="text" class="form-control" name="salesprice_estrippay" id="salesprice_estrippay" placeholder="قیمت فروش استریپ پی" value="{{$managingCurrency[0]->salesprice_estrippay}}">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group m-form__group">
                                    <label for="count_estrippay" class="control-label">تعداد استریپ پی موجود</label>
                                    <input type="text" class="form-control" name="count_estrippay" id="count_estrippay" placeholder="تعداد استریپ پی" value="{{$managingCurrency[0]->count_estrippay}}">
                                </div>
                            </div>











                        </div>
                    </div>
                </div>







                <div class="m-portlet__foot">
                    <div class="m-form__actions">
                        <button type="submit" class="btn btn-success">ویرایش قیمت</button>

                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

        <!--end::Portlet-->


    </div>

@endsection