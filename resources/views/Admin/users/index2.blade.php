@extends('Admin.master')

@section('content')
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        اطلاعات هویتی کاربران
                    </h3>
                </div>
            </div>
        </div>
    <div class="col-sm-9 col-sm-offset-3 col-md-12 col-md-offset-2 main" style="text-align: center">
        <div class="page-header head-section">

            <div class="btn-group">


            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>شناسه</th>
                    <th>نام و نام خانوادگی</th>
                    <th>موبایل</th>
                    <th>تلفن</th>
                    <th>قبض</th>
                    <th>شناسنامه</th>
                    <th>کارت ملی</th>
                    <th>مدیریت</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>
                            @if(  $user->mobile == 0)
                                {{ "اطلاعاتی وارد نشده" }}
                            @else
                                {{ $user->mobile }}
                            @endif
                        </td>
                        <td>
                            @if(  $user->phone == 0)
                                {{ "اطلاعاتی وارد نشده" }}
                            @else
                            {{ $user->phone }}
                        @endif
                        </td>
                        <td>
                            @if(  $user->scane_bill == 0)
                                {{ "اطلاعاتی وارد نشده" }}
                            @elseif(  $user->scane_bill == 1)
                                {{ "ارسال شده" }}
                            @elseif(  $user->scane_bill == 2)
                                {{ "ارسال نشده " }}
                            @elseif(  $user->scane_bill == 3)
                                {{ "ارسال شده و تایید شده" }}
                            @elseif(  $user->scane_bill == 4)
                                {{ "ارسال شده و تایید نشده" }}

                            @endif

                            </td>
                        <td>
                            @if(  $user->scan_birth_certificate == 0)
                                {{ "اطلاعاتی وارد نشده" }}
                            @elseif(  $user->scan_birth_certificate == 1)
                                {{ "ارسال شده" }}
                            @elseif(  $user->scan_birth_certificate == 2)
                                {{ "ارسال نشده " }}
                            @elseif(  $user->scan_birth_certificate == 3)
                                {{ "ارسال شده و تایید شده" }}
                            @elseif(  $user->scan_birth_certificate == 4)
                                {{ "ارسال شده و تایید نشده" }}

                            @endif
                            </td>
                        <td>
                            @if(  $user->scan_national_code == 0)
                                {{ "اطلاعاتی وارد نشده" }}
                            @elseif(  $user->scan_national_code == 1)
                                {{ "ارسال شده" }}
                            @elseif(  $user->scan_national_code == 2)
                                {{ "ارسال نشده " }}
                            @elseif(  $user->scan_national_code == 3)
                                {{ "ارسال شده و تایید شده" }}
                            @elseif(  $user->scan_national_code == 4)
                                {{ "ارسال شده و تایید نشده" }}

                            @endif
                            </td>

                        <td>
                            <form action="{{ route('users.destroy'  , ['id' => $user->id]) }}" method="post">
                                {{ method_field('delete') }}
                                {{ csrf_field() }}
                                <div class="btn-group btn-group-xs">
                                    <button type="submit" class="btn btn-danger">حذف</button>
                                </div>
                            </form>
                        </td>
                        <td>
                            <form action="{{ route('user.verify2'  , ['id' => $user->id]) }}" method="post">

                                {{ csrf_field() }}
                                <div class="btn-group btn-group-xs">
                                    <button type="submit" class="btn btn-primary">تایید شود </button>
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach



                </tbody>
            </table>
            <div style="text-align: center">
                {!! $users->render() !!}
            </div>
        </div>
        {{--<div style="text-align: center">
            {!! $users->render() !!}
        </div>--}}
    </div>
    </div>
@endsection