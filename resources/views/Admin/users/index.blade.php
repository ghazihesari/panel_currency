@extends('Admin.master')

@section('content')
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        لیست کاربران
                    </h3>
                </div>
            </div>
        </div>
    <div class="col-sm-10 col-sm-offset-3 col-md-12 col-md-offset-2 main" style="text-align: center">
        <div class="page-header head-section">

            <div class="btn-group">


            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>شناسه</th>
                    <th>نام و نام خانوادگی</th>
                    <th>ایمیل</th>
                    <th>موبایل</th>
                    <th>تایید هویت</th>
                    <th>تاریخ عضویت</th>
                    <th>وضعیت</th>
                    <th>مدیریت</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->mobile }}</td>
                        <td>0</td>
                        <td>{{ $user->created_at }}</td>

                        <td>

                            <?php
                            if ( $user->level == 1 ) {
                                echo "مدیر";
                            }elseif ( $user->level == 0) {
                                echo "کاربر عادی";
                            }
                            ?>

                        </td>

                        <td>
                            <form action="{{ route('users.destroy'  , ['id' => $user->id]) }}" method="post">
                                {{ method_field('delete') }}
                                {{ csrf_field() }}
                                <div class="btn-group btn-group-xs">
                                    <a href="{{ route('users.edit' , ['id' => $user->id]) }}"  class="btn btn-success" style="margin: 5px">ویرایش</a>
                                    <button type="submit" class="btn btn-danger" style="margin: 5px">حذف</button>
                                </div>
                            </form>
                        </td>
                        <td>
                            <form action="{{ route('user.verify'  , ['id' => $user->id]) }}" method="post">

                                {{ csrf_field() }}
                                <div class="btn-group btn-group-xs">
                                    <button type="submit" class="btn btn-primary">مدیر شود </button>
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach


                </tbody>
            </table>


        </div>
        <div class="row">
            <div class="col-lg-12" align="center">
                {!! $users->render() !!}
            </div>
        </div>

    </div>
    </div>
@endsection