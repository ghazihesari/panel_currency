@extends('Admin.master')

@section('content')
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        لیست گزارش امنیتی کاربران
                    </h3>
                </div>
            </div>
        </div>
    <div class="col-sm-9 col-sm-offset-3 col-md-12 col-md-offset-2 main" style="text-align: center">
        <div class="page-header head-section">

            <div class="btn-group">


            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>کاربر</th>
                    <th>آی پی</th>
                    <th>مرورگر</th>
                    <th>سیستم عامل</th>
                    <th> نام دستگاه</th>
                    <th>عملیات</th>
                    <th>توضیحات</th>
                    <th>تاریخ</th>
                    <th>مدیریت</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $user)

                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->ip }}</td>
                        <td>{{ $user->browser }}</td>
                        <td>{{ $user->os }}</td>
                        <td>
                            <?php
                            if ( $user->device_name == 0 ) {
                                echo "کامپیوتر";
                            }else {
                                return $user->device_name ;
                            }
                            ?>


                            </td>
                        <td>0</td>
                        <td>0</td>
                        <td>{{ $user-> 	created_at  }}</td>



                        <td>
                            <form action="{{ route('users.destroy'  , ['id' => $user->id]) }}" method="post">
                                {{ method_field('delete') }}
                                {{ csrf_field() }}
                                <div class="btn-group btn-group-xs">

                                    <button type="submit" class="btn btn-danger" style="margin: 5px">حذف</button>
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach



                </tbody>
            </table>
            <div style="text-align: center">
                {!! $users->render() !!}
            </div>
        </div>

    </div>
    </div>
@endsection