@extends('Admin.master')

@section('script')

@endsection

@section('content')
    <div class="col-md-12">

        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                        <h3 class="m-portlet__head-text">
                            ویرایش نام کاربری
                        </h3>
                    </div>
                </div>
            </div>

            <!--begin::Form-->
            <form class="form-horizontal" action="{{ route('users.update' , ['id'=>$user->id]) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                @include('Admin.section.errors')
                <div class="m-portlet__body">
                    <div class="container p-0">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="title" class="control-label">نام و نام خانوادگی</label>
                                    <input type="text" class="form-control" name="name" id="name" placeholder="نام و نام خانوادگی خود را وارد نمایید" value="{{ $user->name }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sex" class="control-label">جنسیت</label>
                                    <select name="sex" id="sex" class="form-control">
                                        <option value="1" selected>مرد</option>
                                        <option value="0" selected>زن</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="title" class="control-label">نام پدر</label>
                                    <input type="text" class="form-control" name="father_name" id="father_name" placeholder="نام پدر خود را وارد کنید" value="{{ $user->father_name }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="title" class="control-label">تاریخ تولد</label>
                                    <input type="text" class="form-control" name="date_of_birth" id="date_of_birth" placeholder="تاریخ تولد خود را وارد کنید" value="{{ $user->date_of_birth }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="title" class="control-label">شماره شناسنامه</label>
                                    <input type="text" class="form-control" name="id_birth_certificate" id="id_birth_certificate" placeholder="شماره شناسنامه خود را وارد کنید" value="{{ $user->id_birth_certificate }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="title" class="control-label">کد ملی</label>
                                    <input type="text" class="form-control" name="national_code" id="national_code" placeholder="کد ملی خود را وارد کنید" value="{{ $user->national_code }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="title" class="control-label">تلفن همراه</label>
                                    <input type="text" class="form-control" name="mobile" id="mobile" placeholder="تلفن همراه خود را وارد کنید" value="{{ $user->mobile }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="title" class="control-label">تلفن ثابت</label>
                                    <input type="text" class="form-control" name="phone" id="phone" placeholder="تلفن ثابت خود را وارد کنید" value="{{ $user->phone }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="title" class="control-label">استان</label>
                                    <input type="text" class="form-control" name="province" id="province" placeholder="نام استان خود را وارد کنید" value="{{ $user->province }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="title" class="control-label">شهر</label>
                                    <input type="text" class="form-control" name="city" id="city" placeholder="نام شهر خود را وارد کنید" value="{{ $user->city }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="title" class="control-label">آدرس</label>
                                    <input type="text" class="form-control" name="address" id="address" placeholder="آدرس خود را وارد کنید" value="{{ $user->address }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="title" class="control-label">کد پستی</label>
                                    <input type="text" class="form-control" name="postal_code" id="postal_code" placeholder="کد پستی خود را وارد کنید" value="{{ $user->postal_code }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="title" class="control-label">حداکثر تراکنش روزانه</label>
                                    <input type="text" class="form-control" name="max_daily_transaction" id="max_daily_transaction" placeholder="حداکثر تراکنش روزانه خود را وارد کنید" value="{{ $user->max_daily_transaction }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="title" class="control-label">حداکثر مبلغ خرید روزانه</label>
                                    <input type="text" class="form-control" name="max_daily_purchase_amount" id="max_daily_purchase_amount" placeholder="حداکثر مبلغ خرید روزانه را وارد کنید" value="{{ $user->max_daily_purchase_amount }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="scan_birth_certificate" class="control-label">اسکن شناسنامه</label>
                                    <select name="scan_birth_certificate" id="scan_birth_certificate" class="form-control">
                                        <option value="0" selected>ارسال شده</option>
                                        <option value="1" selected>ارسال نشده</option>
                                        <option value="2" selected>ارسال و تایید شده</option>
                                        <option value="3" selected>ارسال و تایید نشده</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="scan_national_code" class="control-label">اسکن کارت ملی</label>
                                    <select name="scan_national_code" id="scan_national_code" class="form-control">
                                        <option value="1" selected>ارسال شده</option>
                                        <option value="2" selected>ارسال نشده</option>
                                        <option value="3" selected>ارسال و تایید شده</option>
                                        <option value="4" selected>ارسال و تایید نشده</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="scane_bill" class="control-label">اسکن قبض</label>
                                    <select name="scane_bill" id="scane_bill" class="form-control">
                                        <option value="0" selected>ارسال شده</option>
                                        <option value="1" selected>ارسال نشده</option>
                                        <option value="2" selected>ارسال و تایید شده</option>
                                        <option value="3" selected>ارسال و تایید نشده</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="phone_confirmation" class="control-label">تایید تلفن</label>
                                    <select name="phone_confirmation" id="phone_confirmation" class="form-control">
                                        <option value="1" selected> تایید شده</option>
                                        <option value="0" selected> تایید نشده</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="mobile_confirmation" class="control-label">تایید موبایل</label>
                                    <select name="mobile_confirmation" id="mobile_confirmation" class="form-control">
                                        <option value="1" selected> تایید شده</option>
                                        <option value="0" selected> تایید نشده</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="user_status" class="control-label">وضعیت کاربری</label>
                                    <select name="user_status" id="user_status" class="form-control">
                                        <option value="1" selected> فعال</option>
                                        <option value="0" selected> غیر فعال</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="title" class="control-label">ایمیل</label>
                                    <input type="text" class="form-control" name="email" id="email" placeholder="ایمیل خود را وارد کنید" value="{{ $user->email }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="title" class="control-label">کلمه عبور</label>
                                    <input type="text" class="form-control" name="password" id="password" placeholder="پسورد خود را وارد کنید" value="{{ $user->password }}">
                                </div>
                            </div>

                        </div>
                    </div>
                </div>







                <div class="m-portlet__foot">
                    <div class="m-form__actions">
                        <button type="submit" class="btn btn-primary">ویرایش</button>

                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

        <!--end::Portlet-->


    </div>

@endsection