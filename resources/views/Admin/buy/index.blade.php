@extends('Admin.master')

@section('content')
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        لیست سفارش خرید ارز
                    </h3>
                </div>
            </div>
        </div>
    <div class="col-sm-9 col-sm-offset-3 col-md-12 col-md-offset-2 main" style="text-align: center">
        <div class="page-header head-section">

            <div class="btn-group">


                <!--
                      comment
                -->

            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>شناسه</th>
                    <th>نام و نام خانوادگی</th>
                    <th>تعداد ارز دریافتی</th>
                    <th>نوع ارز دریافتی</th>
                    <th>تاریخ</th>
                    <th>فیش بانکی</th>
                    <th>وضعیت</th>
                    <th>مدیریت</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($values as $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                             @foreach($users as $user)
                                <td>{{ $user->name }}</td>
                             @endforeach
                        <td>{{ $value->currency_number }}</td>

                        @if($value->currency_required  == 0)
                            <td>پرفکت مانی</td>
                        @elseif($value->currency_required  == 1)
                            <td>وبمانی</td>
                        @elseif($value->currency_required  == 2)
                            <td>بیت کوین</td>
                        @elseif($value->currency_required  == 3)
                            <td>پی پال</td>
                        @elseif($value->currency_required  == 4)
                            <td>وکس</td>
                        @elseif($value->currency_required  == 5)
                            <td>اگو پی</td>
                        @elseif($value->currency_required  == 6)
                            <td>اوکی پی</td>
                        @elseif($value->currency_required  == 7)
                            <td>پی زا</td>
                        @elseif($value->currency_required  == 8)
                            <td>استریپ پی</td>
                        @elseif($value->currency_required  == 9)
                            <td>دلار</td>
                        @elseif($value->currency_required  == 10)
                            <td>یورو</td>
                        @elseif($value->currency_required  == 11)
                            <td>لیر</td>
                        @elseif($value->currency_required  == 12)
                            <td>پوند</td>
                        @elseif($value->currency_required  == 13)
                            <td>ریال</td>

                        @endif
                                <td>{{ $value->created_at }}</td>
                                <td>{{ $value->sendfishbank }}</td>
                                @if( $value->state == 0 )
                            <td style="background-color: orange;"> تایید نشده است </td>
                        @else
                            <td style="background-color: green;"> توسط مدیر تایید شد </td>
                            @endif</td>

                        <td>
                            <form action="{{ route('adminbuy.delete'  , ['id' => $value->id]) }}" method="post">
                                {{ method_field('delete') }}
                                {{ csrf_field() }}
                                <div class="btn-group btn-group-xs">
                                    <button type="submit" class="btn btn-danger">حذف</button>
                                </div>
                            </form>
                        </td>
                        <td>
                            <form action="{{ route('buy.verify'  , ['id' => $value->id]) }}" method="post">

                                {{ csrf_field() }}
                                <div class="btn-group btn-group-xs">
                                    <button type="submit" class="btn btn-primary">تایید </button>
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach


                </tbody>
            </table>

            <div style="text-align: center">
                {!! $values->render() !!}
            </div>
        </div>

    </div>
    </div>
@endsection