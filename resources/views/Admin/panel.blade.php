@extends('Admin.master')

@section('content')
<div class="m-portlet  m-portlet--unair">
    <div class="m-portlet__body  m-portlet__body--no-padding">
        <div class="row m-row--no-padding m-row--col-separator-xl">
            <div class="col-md-12 col-lg-6 col-xl-3">

                <div class="m-widget24">
                    <div class="m-widget24__item">
                        <h1 class="m-widget24__title">
                            <a href="\admin\panel\user" style="font-size: 21px">کاربران</a>
                        </h1><br>
                        <span class="m-widget24__desc">
                           {{-- <p>تعداد کاربران</p>--}}
                        </span>
                        <span class="m-widget24__stats m--font-brand">
                            {{ $countuser }}
                        </span>
                        <div class="m--space-40"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-6 col-xl-3">

                <div class="m-widget24">
                    <div class="m-widget24__item">
                        <h4 class="m-widget24__title">
                            <a href="\admin\panel\buy" style="font-size: 18px"> سفارشات خرید</a>
                        </h4><br>
                       {{-- <span class="m-widget24__desc">
                            سفارشات سفارشات 
                        </span>--}}
                        <span class="m-widget24__stats m--font-info">
                            {{$countbuy}}
                        </span>
                        <div class="m--space-40"></div>
                    </div>
                </div>

            </div>
            <div class="col-md-12 col-lg-6 col-xl-3">

                <div class="m-widget24">
                    <div class="m-widget24__item">
                        <h4 class="m-widget24__title ">
                            <a href="\admin\panel\sale" style="font-size: 18px; "> سفارشات فروش</a>
                        </h4><br>
                       {{-- <span class="m-widget24__desc">
                            سفارشات فروش
                        </span>--}}
                        <span class="m-widget24__stats m--font-danger">
                            {{$countsale}}
                        </span>
                        <div class="m--space-40"></div>
                    </div>
                </div>

            </div>
            <div class="col-md-12 col-lg-6 col-xl-3">

                <!--begin::New Users-->
                <div class="m-widget24">
                    <div class="m-widget24__item">
                        <h4 class="m-widget24__title">
                            <a href="\admin\panel\ticket" style="font-size: 21px">تیکت ها</a>
                        </h4><br>
                        <span class="m-widget24__desc">

                        </span>
                        <span class="m-widget24__stats m--font-success">
                            {{ $countticket }}
                        </span>
                        <div class="m--space-40"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xl-12">

        <div class="m-portlet m-portlet--full-height  m-portlet--unair">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            لیست سفارشات خرید
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="nav nav-pills nav-pills--brand m-nav-pills--align-right m-nav-pills--btn-pill m-nav-pills--btn-sm" role="tablist">
                        <li class="nav-item m-tabs__item">

                        </li>
                        <li class="nav-item m-tabs__item">

                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="tab-content">
                    <div class="tab-pane active" id="m_widget11_tab1_content">

                        <div class="m-widget11">
                            <div class="table-responsive">

                                <table class="table">

                                    <thead>
                                    <tr>

                                        <th>نام و نام خانوادگی</th>
                                        <th>تعداد ارز دریافتی</th>
                                        <th>نوع ارز دریافتی</th>
                                        <th>تاریخ</th>
                                        <th>وضعیت</th>
                                       {{-- <th>مدیریت</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($values as $value)
                                        <tr>

                                            @foreach($users as $user)
                                                <td>{{ $user->name }}</td>
                                            @endforeach
                                                <td>{{ $value->currency_number }}</td>

                                                @if($value->currency_required  == 0)
                                                    <td>پرفکت مانی</td>
                                                @elseif($value->currency_required  == 1)
                                                    <td>وبمانی</td>
                                                @elseif($value->currency_required  == 2)
                                                    <td>بیت کوین</td>
                                                @elseif($value->currency_required  == 3)
                                                    <td>پی پال</td>
                                                @elseif($value->currency_required  == 4)
                                                    <td>وکس</td>
                                                @elseif($value->currency_required  == 5)
                                                    <td>اگو پی</td>
                                                @elseif($value->currency_required  == 6)
                                                    <td>اوکی پی</td>
                                                @elseif($value->currency_required  == 7)
                                                    <td>پی زا</td>
                                                @elseif($value->currency_required  == 8)
                                                    <td>استریپ پی</td>
                                                @elseif($value->currency_required  == 9)
                                                    <td>دلار</td>
                                                @elseif($value->currency_required  == 10)
                                                    <td>یورو</td>
                                                @elseif($value->currency_required  == 11)
                                                    <td>لیر</td>
                                                @elseif($value->currency_required  == 12)
                                                    <td>پوند</td>
                                                @elseif($value->currency_required  == 13)
                                                    <td>ریال</td>

                                                @endif
                                            <td>{{ $value->created_at }}</td>
                                            <td>{{ $value->user_status }}
                                                @if($value->state == 0)
                                                    {{ "بررسی نشده است" }}
                                                    @else
                                                    {{ "برسی شده است" }}
                                                @endif
                                            </td>

                                          {{--  <td>
                                                <form action="{{ route('buy.destroy'  , ['id' => $value->id]) }}" method="post">
                                                    {{ method_field('delete') }}
                                                    {{ csrf_field() }}
                                                    <div class="btn-group btn-group-xs">
                                                        <button type="submit" class="btn btn-danger">حذف</button>
                                                    </div>
                                                </form>
                                            </td>--}}
                                        </tr>
                                    @endforeach


                                    </tbody>

                                </table>
                            </div>
                        </div>

                    </div>
                   {{-- <div class="tab-pane" id="m_widget11_tab2_content">

                        <div class="m-widget11">
                            <div class="table-responsive">

                                <table class="table">

                                    <thead>
                                    <tr>
                                        <th>شناسه</th>
                                        <th>نام و نام خانوادگی</th>
                                        <th>مبلغ ارز ارسالی</th>
                                        <th>مبلغ ارز دریافتی</th>
                                        <th>تاریخ</th>
                                        <th>وضعیت</th>
                                        <th>مدیریت</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($values as $value)
                                        <tr>
                                            <td>{{ $value->id }}</td>
                                            @foreach($users as $user)
                                                <td>{{ $user->name }}</td>
                                            @endforeach
                                            <td>0</td>
                                            <td>{{ $value->currency_required }}</td>
                                            <td>{{ $value->created_at }}</td>
                                            <td>{{ $value->user_status }}</td>

                                            <td>
                                                <form action="{{ route('buy.destroy'  , ['id' => $value->id]) }}" method="post">
                                                    {{ method_field('delete') }}
                                                    {{ csrf_field() }}
                                                    <div class="btn-group btn-group-xs">
                                                        <button type="submit" class="btn btn-danger">حذف</button>
                                                    </div>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>--}}
                </div>
            </div>
        </div>

    </div>
</div>

@endsection