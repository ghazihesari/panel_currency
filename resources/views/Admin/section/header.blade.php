<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">پنل مدیریت</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            {{--<ul class="nav navbar-nav navbar-right">--}}
                {{--<li><a href="#">Dashboard</a></li>--}}
                {{--<li><a href="#">Settings</a></li>--}}
                {{--<li><a href="#">Profile</a></li>--}}
                {{--<li><a href="#">Help</a></li>--}}
            {{--</ul>--}}
            {{--<form class="navbar-form navbar-right">--}}
                {{--<input type="text" class="form-control" placeholder="Search...">--}}
            {{--</form>--}}

            <div class="navbar-left">
                <a href="/logout" class="btn btn-sm btn-warning" style="margin: 15px">خروج از پنل مدیریت</a>
            </div>
        </div>
    </div>
</nav>

{{--
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">

            <ul class="nav nav-sidebar">
                <li class="active"><a href="/admin/panel">پنل اصلی</a></li>
            </ul>

            <ul class="nav nav-sidebar">
                <li><a href="">کاربران <span class="badge">0</span></a>
                    <ul>
                        <li>
                            <a href="/admin/panel/users/create">اضافه کردن کاربران </a>

                        </li>
                        <li>
                            <a href="/admin/panel/users">مدیریت کاربران </a>

                        </li>
                        <li>
                            <a href="/admin/panel/athen">تایید هویت </a>

                        </li>
                        <li>
                            <a href="">مدیریت کارت بانکی </a>

                        </li>
                    </ul>
                </li>

            </ul>
            <hr>
            <ul class="nav nav-sidebar">
                <li><a href="">مدیران<span class="badge">0</span></a>
                    <ul>
                        <li>
                            <a href="/admin/panel/modir/create">ایجاد مدیر </a>

                        </li>
                        <li>
                            <a href="/admin/panel/modir">مدیریت مدیران </a>

                        </li>
                    </ul>
                </li>
            </ul>
            <hr>
            <ul class="nav nav-sidebar">
                <li><a href="">امور مالی <span class="badge">0</span></a>
                    <ul>
                        <li>
                            <a href="/admin/panel/transaction">لیست تراکنشات </a>
                        </li>
                        <li>
                            <a href="/admin/panel/order">لیست سفارشات </a>
                        </li>
                        <li>
                            <a href="/admin/panel/buy">سفارشات خرید </a>
                        </li>
                        <li>
                            <a href="/admin/panel/sale">سفارشات فروش </a>
                        </li>
                        <li>
                            <a href="">سفارشات ووچر </a>
                        </li>

                    </ul>
                </li>
            </ul>
            <hr>
            <ul class="nav nav-sidebar">
                <li><a href="">گزارش امنیتی <span class="badge">0</span></a>
                    <ul>
                        <li>
                            <a href="/admin/panel/usersecreport">گزارش امنیتی کاربران</a>
                        </li>
                        <li>
                            <a href="/admin/panel/modirsecreport">گزارش امنیتی مدیران </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <hr>
            <ul class="nav nav-sidebar">
                <li><a href="/admin/panel/ticket">مدیریت تیکت ها <span class="badge">0</span></a></li>
            </ul>
            <hr>
            <ul class="nav nav-sidebar">
                <li><a href="">تنظیمات <span class="badge">0</span></a>
                    <ul>
                        <li>
                            <a href="">مدیریت نرخ ارز ها </a>
                        </li>
                        <li>
                            <a href="">مدیریت حساب ارزی </a>
                        </li>
                        <li>
                            <a href="">وضعیت خرید و فروش ارز </a>
                        </li>
                        <li>
                            <a href="">درگاه ریالی </a>
                        </li>
                        <li>
                            <a href="">پیکربندی عمومی </a>
                        </li>
                        <li>
                            <a href="">اطلاع رسانی ها </a>
                        </li>
                    </ul>
                </li>
            </ul>




        </div>--}}
