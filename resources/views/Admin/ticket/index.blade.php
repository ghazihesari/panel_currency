@extends('Admin.master')

@section('content')
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        لیست تیکت ها
                    </h3>
                </div>
            </div>
        </div>
    <div class="col-sm-9 col-sm-offset-3 col-md-12 col-md-offset-2 main" style="text-align: center">
        <div class="page-header head-section">

            <div class="btn-group">


            </div>
        </div>
        <div class="table-responsive">

            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>شناسه</th>
                    <th>نام و نام خانوادگی</th>
                    <th>عنوان</th>
                    <th>واحد پشتیبانی</th>
                    <th>اولویت</th>
                    <th>تاریخ</th>
                    <th>وضعیت</th>
                    <th>مدیریت</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <tbody>


                @foreach($tickets2 as $ticket2)
                    <tr>
                        <td>{{ $ticket2->id }}</td>
                        <td>
                            <?php
                                $name=\App\User::where('id',$ticket2->user_id )->get();
                               // dd($name[0]);
                            ?>
                            {{ $name[0]->name }}
                        </td>
                        <td>{{ $ticket2->title }}</td>

                        <td>
                            @if(  $ticket2->support_unit == 0)
                                {{ "سفارشات" }}
                            @elseif(  $ticket2->support_unit == 1)
                                {{ "امور مالی" }}
                            @elseif(  $ticket2->support_unit == 2)
                                {{ "امور فنی" }}
                            @elseif(  $ticket2->support_unit == 3)
                                {{ "مدیریت" }}
                            @elseif(  $ticket2->support_unit == 4)
                                {{ "متفرقه" }}
                            @endif
                        </td>
                        <td>
                            @if(  $ticket2->priority == 0)
                                {{ "فوری" }}
                            @elseif(  $ticket2->priority == 1)
                                {{ "مهم" }}
                            @elseif(  $ticket2->priority == 2)
                                {{ "متوسط" }}
                            @elseif(  $ticket2->priority == 3)
                                {{ "معمولی" }}

                            @endif
                        </td>
                        <td>{{ $ticket2->created_at }}</td>
                        @if( $ticket2->state == 0 )
                            <td style="background-color: orange;"> بررسی نشده است </td>
                        @else
                            <td style="background-color: green;"> توسط مدیر بررسی شد </td>
                        @endif

                        <td>
                            <form action="{{ route('adminticket.delete'  , ['id' => $ticket2->id]) }}" method="post">
                                {{ method_field('delete') }}
                                {{ csrf_field() }}
                                <div class="btn-group btn-group-xs">
                                    <button type="submit" class="btn btn-danger">حذف</button>

                                </div>
                            </form>
                        </td>
                        <td>
                            <form action="{{ route('adminticket.verify'  , ['id' => $ticket2->id]) }}" method="post">

                                {{ csrf_field() }}
                                <div class="btn-group btn-group-xs">
                                    <button type="submit" class="btn btn-primary">تایید </button>
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach

                       {{-- <script type="text/javascript">
                            function del(id) {
                                alert(id);
                                
                            }
                        </script>--}}


                </tbody>
            </table>
            <div style="text-align: center">
                {!! $tickets2->render() !!}
            </div>
        </div>

    </div>
    </div>
@endsection