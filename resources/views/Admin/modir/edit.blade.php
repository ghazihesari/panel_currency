@extends('Admin.master')

@section('script')

@endsection

@section('content')
    <div class="col-md-12">

        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                        <h3 class="m-portlet__head-text">
                            ویرایش مدیر
                        </h3>
                    </div>
                </div>
            </div>

            <!--begin::Form-->

     <?php
        // dd($users[0]->id);
      ?>

            <form class="form-horizontal" action="{{ route('modir.update' , ['id'=>$user->id]) }}) }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                @include('Admin.section.errors')


                <div class="m-portlet__body">
                    <div class="container p-0">
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="name" class="control-label">نام و نام خانوادگی</label>
                                        <input type="text" class="form-control" name="name" id="name" placeholder="نام و نام خانوادگی خود را وارد نمایید" value="{{ $user->name }}">
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="sex" class="control-label">جنسیت</label>
                                        <select name="sex" id="sex" class="form-control">
                                            <option value="1" selected>مرد</option>
                                            <option value="0" selected>زن</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="father_name" class="control-label">نام پدر</label>
                                        <input type="text" class="form-control" name="father_name" id="father_name" placeholder="نام پدر خود را وارد کنید" value="{{ $user->father_name }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="date_of_birth" class="control-label">تاریخ تولد</label>
                                        <input type="text" class="form-control" name="date_of_birth" id="date_of_birth" placeholder="تاریخ تولد خود را وارد کنید" value="{{$user->date_of_birth }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="id_birth_certificate" class="control-label">شماره شناسنامه</label>
                                        <input type="text" class="form-control" name="id_birth_certificate" id="id_birth_certificate" placeholder="شماره شناسنامه خود را وارد کنید" value="{{ $user->id_birth_certificate }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="national_code" class="control-label">کد ملی</label>
                                        <input type="text" class="form-control" name="national_code" id="national_code" placeholder="کد ملی خود را وارد کنید" value="{{ $user->national_code }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="mobile" class="control-label">تلفن همراه</label>
                                        <input type="text" class="form-control" name="mobile" id="mobile" placeholder="تلفن همراه خود را وارد کنید" value="{{ $user->mobile }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="phone" class="control-label">تلفن ثابت</label>
                                        <input type="text" class="form-control" name="phone" id="phone" placeholder="تلفن ثابت خود را وارد کنید" value="{{ $user->phone }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="province" class="control-label">استان</label>
                                        <input type="text" class="form-control" name="province" id="province" placeholder="نام استان خود را وارد کنید" value="{{ $user->province }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="city" class="control-label">شهر</label>
                                        <input type="text" class="form-control" name="city" id="city" placeholder="نام شهر خود را وارد کنید" value="{{ $user->city }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="address" class="control-label">آدرس</label>
                                        <input type="text" class="form-control" name="address" id="address" placeholder="آدرس خود را وارد کنید" value="{{ $user->address }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="postal_code" class="control-label">کد پستی</label>
                                        <input type="text" class="form-control" name="postal_code" id="postal_code" placeholder="کد پستی خود را وارد کنید" value="{{ $user->postal_code }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="max_daily_transaction" class="control-label">حداکثر تراکنش روزانه</label>
                                        <input type="text" class="form-control" name="max_daily_transaction" id="max_daily_transaction" placeholder="حداکثر تراکنش روزانه خود را وارد کنید" value="{{ $user->max_daily_transaction }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="max_daily_purchase_amount" class="control-label">حداکثر مبلغ خرید روزانه</label>
                                        <input type="text" class="form-control" name="max_daily_purchase_amount" id="max_daily_purchase_amount" placeholder="حداکثر مبلغ خرید روزانه را وارد کنید" value="{{ $user->max_daily_purchase_amount }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="scan_birth_certificate" class="control-label">اسکن شناسنامه</label>
                                        <select name="scan_birth_certificate" id="scan_birth_certificate" class="form-control">
                                            <option value="0" selected>ارسال شده</option>
                                            <option value="1" selected>ارسال نشده</option>
                                            <option value="2" selected>ارسال و تایید شده</option>
                                            <option value="3" selected>ارسال و تایید نشده</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="scan_national_code" class="control-label">اسکن کارت ملی</label>
                                        <select name="scan_national_code" id="scan_national_code" class="form-control">
                                            <option value="1" selected>ارسال شده</option>
                                            <option value="2" selected>ارسال نشده</option>
                                            <option value="3" selected>ارسال و تایید شده</option>
                                            <option value="4" selected>ارسال و تایید نشده</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="scane_bill" class="control-label">اسکن قبض</label>
                                        <select name="scane_bill" id="scane_bill" class="form-control">
                                            <option value="0" selected>ارسال شده</option>
                                            <option value="1" selected>ارسال نشده</option>
                                            <option value="2" selected>ارسال و تایید شده</option>
                                            <option value="3" selected>ارسال و تایید نشده</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="phone_confirmation" class="control-label">تایید تلفن</label>
                                        <select name="phone_confirmation" id="phone_confirmation" class="form-control">
                                            <option value="1" selected> تایید شده</option>
                                            <option value="0" selected> تایید نشده</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="mobile_confirmation" class="control-label">تایید موبایل</label>
                                        <select name="mobile_confirmation" id="mobile_confirmation" class="form-control">
                                            <option value="1" selected> تایید شده</option>
                                            <option value="1" selected> تایید نشده</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="user_status" class="control-label">وضعیت کاربری</label>
                                        <select name="user_status" id="user_status" class="form-control">
                                            <option value="1" selected> فعال</option>
                                            <option value="0" selected> غیر فعال</option>


                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="create_user" class="control-label">ایجاد کاربر</label>
                                        <select name="create_user" id="create_user" class="form-control">
                                            <option value="1" selected> فعال</option>
                                            <option value="0" selected> غیر فعال</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="edit_user" class="control-label">ویرایش کاربر</label>
                                        <select name="edit_user" id="edit_user" class="form-control">
                                            <option value="1" selected> فعال</option>
                                            <option value="0" selected> غیر فعال</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="create_admin" class="control-label">ایجاد مدیر</label>
                                        <select name="create_admin" id="create_admin" class="form-control">
                                            <option value="1" selected> فعال</option>
                                            <option value="0" selected> غیر فعال</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="edit_admin" class="control-label">ویرایش مدیر</label>
                                        <select name="edit_admin" id="edit_admin" class="form-control">
                                            <option value="1" selected> فعال</option>
                                            <option value="0" selected> غیر فعال</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="verify_users" class="control-label">تایید کاربران</label>
                                        <select name="verify_users" id="verify_users" class="form-control">
                                            <option value="1" selected> فعال</option>
                                            <option value="0" selected> غیر فعال</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="ticket_support" class="control-label">تیکت پشتیبانی</label>
                                        <select name="ticket_support" id="ticket_support" class="form-control">
                                            <option value="1" selected> فعال</option>
                                            <option value="0" selected> غیر فعال</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="notifications" class="control-label">اطلاع رسانی ها</label>
                                        <select name="notifications" id="notifications" class="form-control">
                                            <option value="1" selected> فعال</option>
                                            <option value="0" selected> غیر فعال</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="security_reports" class="control-label">گزارش امنیتی</label>
                                        <select name="security_reports" id="security_reports" class="form-control">
                                            <option value="1" selected> فعال</option>
                                            <option value="0" selected> غیر فعال</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="list_of_transactions" class="control-label">لیست تراکنشات</label>
                                        <select name="list_of_transactions" id="list_of_transactions" class="form-control">
                                            <option value="1" selected> فعال</option>
                                            <option value="0" selected> غیر فعال</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="orders_list" class="orders_list-label">لیست سفارشات</label>
                                        <select name="orders_list" id="list_of_transactions" class="form-control">
                                            <option value="1" selected> فعال</option>
                                            <option value="0" selected> غیر فعال</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="manage_orders" class="orders_list-label">مدیریت سفارشات</label>
                                        <select name="manage_orders" id="manage_orders" class="form-control">
                                            <option value="1" selected> فعال</option>
                                            <option value="0" selected> غیر فعال</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="currency_accounts" class="orders_list-label">حساب های ارزی</label>
                                        <select name="currency_accounts" id="currency_accounts" class="form-control">
                                            <option value="1" selected> فعال</option>
                                            <option value="0" selected> غیر فعال</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="currency_trading_status" class="orders_list-label">وضعیت خرید و فروش ارز</label>
                                        <select name="currency_trading_status" id="currency_trading_status" class="form-control">
                                            <option value="1" selected> فعال</option>
                                            <option value="0" selected> غیر فعال</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="currency" class="orders_list-label">نرخ ارزها</label>
                                        <select name="currency" id="currency" class="form-control">
                                            <option value="1" selected> فعال</option>
                                            <option value="0" selected> غیر فعال</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="rial_accounts" class="orders_list-label">حساب های ریالی</label>
                                        <select name="rial_accounts" id="rial_accounts" class="form-control">
                                            <option value="1" selected> فعال</option>
                                            <option value="0" selected> غیر فعال</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="internal_ports" class="orders_list-label">درگاه های داخلی</label>
                                        <select name="internal_ports" id="internal_ports" class="form-control">
                                            <option value="1" selected> فعال</option>
                                            <option value="0" selected> غیر فعال</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="system_settings" class="orders_list-label">تنظیمات سیستم</label>
                                        <select name="system_settings" id="system_settings" class="form-control">
                                            <option value="1" selected> فعال</option>
                                            <option value="0" selected> غیر فعال</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">



                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="email" class="control-label">ایمیل</label>
                                        <input type="text" class="form-control" name="email" id="email" placeholder="ایمیل خود را وارد کنید" value="{{ $user->email }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group" m-form__group>
                                    <div class="col-sm-12">
                                        <label for="password" class="control-label">کلمه عبور</label>
                                        <input type="text" class="form-control" name="password" id="password" placeholder="پسورد خود را وارد کنید" value="{{ $user->password }}">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="m-portlet__foot">
                    <div class="m-form__actions">
                        <button type="submit" class="btn btn-primary">ویرایش</button>
                        {{--<button type="reset" class="btn btn-secondary">ریست</button>--}}
                    </div>
                </div>
            </form>

        </div>

        <!--end::Form-->
    </div>

    <!--end::Portlet-->


    </div>

@endsection