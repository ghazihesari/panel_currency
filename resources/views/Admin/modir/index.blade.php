@extends('Admin.master')

@section('content')
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        لیست مدیران
                    </h3>
                </div>
            </div>
        </div>

    <div class="col-sm-9 col-sm-offset-3 col-md-12 col-md-offset-2 main" style="text-align: center">
        <div class="page-header head-section">

            <div class="btn-group">


            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr >
                    <th>شناسه</th>
                    <th>نام و نام خانوادگی</th>
                    <th>ایمیل</th>
                    <th>تایید هویت</th>
                    <th>تاریخ عضویت</th>
                   {{-- <th>وضعیت</th>--}}
                    <th >مدیریت</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($admins as $admin)
                    <tr>
                        <td>{{ $admin->id }}</td>
                        <td>{{ $admin->name }}</td>
                        <td>{{ $admin->email }}</td>
                        <td>0</td>
                        <td>{{ $admin->created_at }}</td>

                     {{--   @if( $admin->user_status == 0 )
                            <td style="background-color: orange;"> غیر فعال </td>
                        @else
                            <td style="background-color: green;"> فعال </td>
                            @endif--}}


                        <td>
                            <form action="{{ route('modir.destroy'  , ['id' => $admin->id]) }}" method="post">
                                {{ method_field('delete') }}
                                {{ csrf_field() }}
                                <div class="btn-group btn-group-xs">
                                    <a href="{{ route('modir.edit' , ['id' => $admin->id]) }}"  class="btn btn-success" style="margin: 5px">ویرایش</a>
                                    <button type="submit" class="btn btn-danger" style="margin: 5px">حذف</button>

                                </div>

                            </form>
                        </td>


                    </tr>
                @endforeach


                </tbody>
            </table>
            <div style="text-align: center">
                {!! $admins->render() !!}
            </div>
        </div>

    </div>
    </div>
@endsection