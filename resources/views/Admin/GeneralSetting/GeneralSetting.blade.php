@extends('Admin.master')

@section('script')

@endsection

@section('content')
    <div class="col-md-12">

        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                        <h3 class="m-portlet__head-text">
                            تنظیمات عمومی
                        </h3>
                    </div>
                </div>
            </div>

            <!--begin::Form-->
            <form class="form-horizontal" action="{{ route('setting.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="m-portlet__body">
                    <div class="container p-0">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="sitename" class="control-label">نام سایت:</label>
                                    <input type="text" class="form-control" name="sitename" id="sitename" placeholder="خرید و فروش اتوماتیک ارز های دیجیتال" value="{{ old('sitename') }}">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="companyname" class="control-label">نام کمپانی :</label>
                                    <input type="text" class="form-control" name="companyname" id="companyname" placeholder="زرین اکسچنج شما" value="{{ old('companyname') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="supportemail" class="control-label">ایمیل پشتیبانی :</label>
                                    <input type="text" class="form-control" name="supportemail" id="supportemail" placeholder="ایمیل پشتیبانی را وارد کنید" value="{{ old('supportemail') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="adminemail" class="control-label">ایمیل مدیریت :</label>
                                    <input type="text" class="form-control" name="adminemail" id="adminemail" placeholder="ایمیل مدیریت را وارد کنید" value="{{ old('adminemail') }}">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="tell" class="control-label">تلفن ثابت :</label>
                                    <input type="text" class="form-control" name="tell" id="tell" placeholder="تلفن ثابت را وارد کنید" value="{{ old('tell') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="phone" class="control-label">تلفن همراه</label>
                                    <input type="text" class="form-control" name="phone" id="phone" placeholder="تلفن همراه خود را وارد کنید" value="{{ old('phone') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="addresscompany" class="control-label">آدرس کمپانی :</label>
                                    <input type="text" class="form-control" name="addresscompany" id="addresscompany" placeholder="آدرس کمپانی را وارد کنید" value="{{ old('addresscompany') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="countbuy_day" class="control-label">تعداد خرید روزانه :</label>
                                    <input type="text" class="form-control" name="countbuy_day" id="countbuy_day" placeholder="سقف تعداد سفارش مجاز روزانه هر کاربر" value="{{ old('countbuy_day') }}">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="intercambios_day" class="control-label">سقف تبادل ریالی روزانه :</label>
                                    <input type="text" class="form-control" name="intercambios_day" id="intercambios_day" placeholder="سقف تبادل ریالی روزانه" value="{{ old('intercambios_day') }}">
                                </div>
                            </div>
                           {{-- <div class="col-lg-8">
                                <div class="fileupload fileupload-new" data-provides="fileupload">

                                    <div>

                            <span class="btn btn-file "><span class="fileupload-new"> لوگو کمپانی   : </span>
                                <input type="file" name="companylogo" /></span>

                                    </div>
                                </div>
                            </div>--}}
                            <div class="form-group">
                                <label class="col-md-8 control-label"></i>لوگو کمپانی   : </label>
                                <div class="input-group input-group-sm">
                                    <input name="companylogo" class="" type="file" required
                                    >
                                    <br>
                                </div>
                            </div>










                        </div>
                    </div>
                </div>







                <div class="m-portlet__foot">
                    <div class="m-form__actions">
                        <button type="submit" class="btn btn-success">ویرایش تنظیمات عمومی</button>

                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

        <!--end::Portlet-->


    </div>

@endsection