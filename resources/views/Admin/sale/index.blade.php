@extends('Admin.master')

@section('content')
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        لیست سفارش فروش ارز
                    </h3>
                </div>
            </div>
        </div>
    <div class="col-sm-9 col-sm-offset-3 col-md-12 col-md-offset-2 main" style="text-align: center">
        <div class="page-header head-section">

            <div class="btn-group">


            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>شناسه</th>
                    <th>نام و نام خانوادگی</th>
                    <th>نوع ارز ارسالی</th>
                    <th>تاریخ</th>
                    <th>وضعیت</th>
                    <th>مدیریت</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                @foreach($values as $value)
                    <tr>
                        <td>{{ $value->id }}</td>
                        @foreach($users as $user)
                            <td>{{ $user->name }}</td>
                        @endforeach


                        @if($value->submitted_currency  == 0)
                            <td>پرفکت مانی</td>
                        @elseif($value->submitted_currency  == 1)
                            <td>وبمانی</td>
                        @elseif($value->submitted_currency  == 2)
                            <td>بیت کوین</td>
                        @elseif($value->submitted_currency  == 3)
                            <td>پی پال</td>
                        @elseif($value->submitted_currency  == 4)
                            <td>وکس</td>
                        @elseif($value->submitted_currency  == 5)
                            <td>اگو پی</td>
                        @elseif($value->submitted_currency  == 6)
                            <td>اوکی پی</td>
                        @elseif($value->submitted_currency  == 7)
                            <td>پی زا</td>
                        @elseif($value->submitted_currency  == 8)
                            <td>استریپ پی</td>
                        @elseif($value->submitted_currency  == 9)
                            <td>دلار</td>
                        @elseif($value->submitted_currency  == 10)
                            <td>یورو</td>
                        @elseif($value->submitted_currency  == 11)
                            <td>لیر</td>
                        @elseif($value->submitted_currency  == 12)
                            <td>پوند</td>
                        @elseif($value->submitted_currency  == 13)
                            <td>ریال</td>

                        @endif


                        <td>{{ $value->created_at }}</td>
                        @if( $value->state == 0 )
                            <td style="background-color: orange;"> تایید نشده است </td>
                        @else
                            <td style="background-color: green;"> توسط مدیر تایید شد </td>
                            @endif</td>

                        <td>
                            <form action="{{ route('adminsale.delete'  , ['id' => $value->id]) }}" method="post">
                                {{ method_field('delete') }}
                                {{ csrf_field() }}
                                <div class="btn-group btn-group-xs">
                                    <button type="submit" class="btn btn-danger">حذف</button>
                                </div>
                            </form>
                        </td>
                        <td>
                            <form action="{{ route('sale.verify'  , ['id' => $value->id]) }}" method="post">

                                {{ csrf_field() }}
                                <div class="btn-group btn-group-xs">
                                    <button type="submit" class="btn btn-primary">تایید </button>
                                </div>
                            </form>
                        </td>
                    </tr>
                @endforeach



                </tbody>
            </table>
            <div style="text-align: center">
                {!! $values->render() !!}
            </div>
        </div>

    </div>
    </div>
@endsection