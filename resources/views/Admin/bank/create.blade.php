@extends('Admin.master')

@section('script')

@endsection

@section('content')
    <div class="col-md-12">

        <!--begin::Portlet-->
        <div class="m-portlet m-portlet--tab">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                        <h3 class="m-portlet__head-text">
                            اضافه کردن بانک
                        </h3>
                    </div>
                </div>
            </div>

            <!--begin::Form-->
            <form class="form-horizontal" action="{{ route('bank.store') }}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                @include('Admin.section.errors')
                <div class="m-portlet__body">
                    <div class="container p-0">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group m-form__group">
                                    <label for="bankname" class="control-label">نام بانک</label>
                                    <input type="text" class="form-control" name="bankname" id="bankname" placeholder="نام بانک خود را وارد نمایید" value="{{ old('bankname') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="m-portlet__foot">
                    <div class="m-form__actions">
                        <button type="submit" class="btn btn-primary">ارسال</button>

                    </div>
                </div>
            </form>

            <!--end::Form-->
        </div>

        <!--end::Portlet-->


    </div>

@endsection