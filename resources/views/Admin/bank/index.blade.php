@extends('Admin.master')

@section('content')
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <span class="m-portlet__head-icon m--hide">
                        <i class="la la-gear"></i>
                    </span>
                    <h3 class="m-portlet__head-text">
                        لیست بانک های وارد شده
                    </h3>
                </div>
            </div>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-12 col-md-offset-2 main" style="text-align: center">
            <div class="page-header head-section">

                <div class="btn-group">


                    <!--
                          comment
                    -->

                </div>
            </div>
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>شناسه</th>
                        <th>نام بانک</th>
                        <th>تاریخ</th>
                        <th>مدیریت</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($banks as $bank)
                        <tr>
                            <td>{{ $bank->id }}</td>

                            <td>{{ $bank->bankname }}</td>
                            <td>{{ $bank->created_at }}</td>

                                <td>
                                    <form action="{{ route('bank.destroy'  , ['id' => $bank->id]) }}" method="post">
                                        {{ method_field('delete') }}
                                        {{ csrf_field() }}
                                        <div class="btn-group btn-group-xs">
                                            <button type="submit" class="btn btn-danger">حذف</button>
                                        </div>
                                    </form>
                                </td>

                        </tr>
                    @endforeach


                    </tbody>
                </table>

                <div style="text-align: center">
                    {!! $banks->render() !!}
                </div>
            </div>

        </div>
    </div>
@endsection