window.$ = window.jQuery = require('jquery');
import Inputmask from "inputmask";
import * as moment from 'moment';
import swal from 'sweetalert';
window.swal = swal;

require('bootstrap');
require('bootstrap-switch');
require('./bootstrap-datepicker');
require('./admin/vendors.bundle');
require('./admin/scripts.bundle');



import PerfectScrollbar from 'perfect-scrollbar';
window.PerfectScrollbar = PerfectScrollbar;
