<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ManagingCurrency extends Model
{
    protected $fillable = [
        'id',
        'purchaseprice_perfectmani','purchaseprice_webmani','purchaseprice_bitcoin','purchaseprice_paypal',
        'purchaseprice_vax','purchaseprice_egopay','purchaseprice_okpay','purchaseprice_payza',
        'purchaseprice_estrippay','purchaseprice_dolar','purchaseprice_yoro','purchaseprice_lir'
        ,'purchaseprice_pond','purchaseprice_rial',
        'salesprice_perfectmani','salesprice_webmani','salesprice_bitcoin','salesprice_paypal',
        'salesprice_vax','salesprice_egopay','salesprice_okpay','salesprice_payza',
        'salesprice_estrippay','salesprice_dolar','salesprice_yoro','salesprice_lir'
        ,'salesprice_pond','salesprice_rial',
        'count_perfectmani','count_webmani','count_bitcoin','count_paypal',
        'count_vax','count_egopay','count_okpay','count_payza',
        'count_estrippay','count_dolar','count_yoro','count_lir'
        ,'count_pond','count_rial',


    ];
}
