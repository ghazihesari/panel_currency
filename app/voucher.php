<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class voucher extends Model
{
    //
    protected $fillable = [
        'user_id','voucher_type','voucher_number','voucher_code',
        'price_rial','price_arz','status',


    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
