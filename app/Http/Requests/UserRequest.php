<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> 'required',
            'sex'=> 'required',
            'father_name'=> 'required',
            'date_of_birth'=> 'required',
            'id_birth_certificate'=> 'required',
            'national_code'=> 'required',
            'mobile'=> 'required',
            'phone'=> 'required',
            'province'=> 'required',
            'city'=> 'required',
            'address'=> 'required',
            'postal_code'=> 'required',
            'email'=> 'required',
            'password'=> 'required',



        ];
    }
}
