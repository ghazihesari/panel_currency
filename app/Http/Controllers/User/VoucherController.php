<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\ManagingCurrency;
use App\User;
use App\voucher;
use Illuminate\Http\Request;
use SoapClient;


class VoucherController extends Controller
{
    protected $MerchantID = 'f83cc956-f59f-11e6-889a-005056a205be';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('user.voucher.listvoucher');
    }
    public function indexadmin()
    {
        $values=voucher::latest()->paginate(12);
        //  dd($values->toArray()); die;
        $user_id=$values[0]['user_id'];
        // dd($test);die;
        $users=User::where('id',$user_id)->get();
      //   dd($values);
        return view('Admin.voucher.index',compact('values','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $price=ManagingCurrency::get();
        //  dd($price[0]);
        $cards=\App\account_information::where('user_id',auth()->user()->id)->get();
        $i=0;
        return view('user.voucher.voucher_buy' , compact('price','cards' , 'i'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function show(voucher $voucher)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function edit(voucher $voucher)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, voucher $voucher)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\voucher  $voucher
     * @return \Illuminate\Http\Response
     */
    public function destroy(voucher $voucher)
    {
        //
    }

    public function payment()
    {

        $Amount = 100; //Amount will be based on Toman - Required
        $Description = 'توضیحات تراکنش تستی'; // Required
        $Email = auth()->user()->email; // Optional
        $Mobile = '09123456789'; // Optional
        $CallbackURL = 'http://localhost:8000/user/panel/sale/checker'; // Required


        $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);

        $result = $client->PaymentRequest(
            [

                'MerchantID' => $this->MerchantID,
                'Amount' => $Amount,
                'Description' => $Description,
                'Email' => $Email,
                'Mobile' => $Mobile,
                'CallbackURL' => $CallbackURL,
            ]
        );

        if ($result->Status == 100) {

            auth()->user()->buy()->create([
                'resnumber'=> $result->Authority,
                'currency_required'=> 1,
                'currency_number'=>2,
                'accountـreceived'=>3,
                'total_price'=>4,
                'orderـtype'=>'5',
                'state'=>'6',

            ]);

            return redirect('https://www.zarinpal.com/pg/StartPay/'.$result->Authority);
        } else {
            echo'ERR: '.$result->Status;
        }
    }
    public function checker()
    {
        $Authority=request('Authority');
        $payment=sale::whereResnumber($Authority)->firstOrFaild();
        if (request('Status') == 'OK') {

            $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);

            $result = $client->PaymentVerification(
                [
                    'MerchantID' => $this->MerchantID,
                    'Authority' => $Authority,
                    'Amount' => $payment->price,
                ]
            );
            if ($result->Status == 100) {
                echo 'Transaction success. RefID:'.$result->RefID;
            } else {
                echo 'Transaction failed. Status:'.$result->Status;
            }
        } else {
            echo 'Transaction canceled by user';


        }
    }
}
