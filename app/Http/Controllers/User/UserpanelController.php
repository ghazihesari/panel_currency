<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\userpanel;
use Illuminate\Http\Request;

class UserpanelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       /* return "32121200";*/


        return view('user.master');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\userpanel  $userpanel
     * @return \Illuminate\Http\Response
     */
    public function show(userpanel $userpanel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\userpanel  $userpanel
     * @return \Illuminate\Http\Response
     */
    public function edit(userpanel $userpanel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\userpanel  $userpanel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, userpanel $userpanel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\userpanel  $userpanel
     * @return \Illuminate\Http\Response
     */
    public function destroy(userpanel $userpanel)
    {
        //
    }
}
