<?php

namespace App\Http\Controllers\user;

use App\account_information;
use App\Bank;
use App\Http\Controllers\Controller;
use App\Http\Requests\AccountInfoRequest;
use App\User;
use Illuminate\Http\Request;

class AccountInformationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=auth()->user()->id;
        $cards=account_information::where('user_id',$user)->latest()->paginate(5);
        return view('user.accountinformation.index',compact('cards' ));
    }
    public function indexadmin()
    {
       // dd('654');
        $users=User::get();
        $cards=account_information::latest()->paginate(5);
        return view('Admin.accountinformation.index',compact('cards' , 'users'));
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $values=Bank::latest()->get();
        $i=0;
        return view('user.accountinformation.create' , compact('values' , 'i'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AccountInfoRequest $request)
    {

       // dd($request->all());

        $card=$request->all();
        $card['user_id']= auth()->user()->id;
        $card['state']='0';


        if ( $request->hasFile('imgcard') ) {
            $FileName = time().'.'.$request->file('imgcard')->getClientOriginalExtension();
            if ( $request->file('imgcard')->move( 'assets/imagecard',$FileName ) ) {

                //  dd($FileName);

                $card['scan_card']=$FileName;
                $card['status_scan_card']='ارسال شده';



                $card = account_information::create($card);

                alert()->success('', ' افزودن کارت جدید انجام شد ')->persistent('Close');

                return back();

            }



        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\account_information  $account_information
     * @return \Illuminate\Http\Response
     */
    public function show(account_information $account_information)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\account_information  $account_information
     * @return \Illuminate\Http\Response
     */
    public function edit(account_information $account_information)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\account_information  $account_information
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, account_information $account_information)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\account_information  $account_information
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //dd($id);
        $account_information=account_information::where('user_id',auth()->user()->id)->get();
        $account_information=account_information::find($id);
        $account_information->delete();
        //  $help->delete();
        return back();
    }
    public function destroy2($id)
    {
       // dd($id);
        $account_information=account_information::find($id);
        $account_information->delete();
        //  $help->delete();
        return back();
    }
    public function verifyaccount($id)
    {
        $acc=account_information::find($id);
        $acc->update([
            'state'=>'1'
        ]);
        return back();
    }



}
