<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Http\Requests\TicketRequest;
use App\ticket;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index2()
    {

        $tickets2=ticket::latest()->paginate(5);
      //  dd($tickets2);
        return view('Admin.ticket.index',compact('tickets2'));
    }
    public function index()
    {
        $user=auth()->user()->id;

        $tickets1=ticket::where('user_id',$user)->latest()->paginate(5);

        return view('user.ticket.index',compact('tickets1'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('user.ticket.sendticket');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TicketRequest $request)
    {



            $ticket=$request->all();


        if ( $request->hasFile('imgticket') ) {
            $FileName = time().'.'.$request->file('imgticket')->getClientOriginalExtension();
            if ( $request->file('imgticket')->move( 'assets/imageticket',$FileName ) ) {

              //  dd($FileName);

                $ticket['attached']=$FileName;
                $ticket['user_id']=auth()->user()->id;;

                $ticket['state']='0';

                $ticket = ticket::create($ticket);
                alert()->success('', ' تیکت ارسال شد ')->persistent('Close');
                return back();
            }



        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function show(ticket $ticket)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function edit(ticket $ticket)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ticket $ticket)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function destroy(ticket $ticket)
    {
        $tick=\App\ticket::find($ticket);
        //  dd($sale1->toArray());
        $tick[0]->delete();

        return back();
    }
    public function destroy2($id)
    {
      // dd($id);
        $ticket=ticket::find($id);
       $ticket->delete();
       return back();

    }
    public function verify($id)
    {
        //  dd($id);
        $ticket=ticket::find($id);
        $ticket->update([
            'state'=>'1'
        ]);
        return back();

    }
}
