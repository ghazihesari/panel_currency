<?php

namespace App\Http\Controllers\user;

use App\account_information;
use App\buy;
use App\Http\Controllers\Controller;

use App\Http\Requests\BuyRequest;
use App\ManagingCurrency;
use App\User;
use Illuminate\Http\Request;
use SoapClient;


class BuyController extends Controller
{

    protected $MerchantID = 'f83cc956-f59f-11e6-889a-005056a205be';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public  function index()
    {
        $user=auth()->user()->id;

        $values=buy::where('user_id',$user)->latest()->paginate(5);
        // dd($values->toArray()); die;
        $user_id=$values[0]['user_id'];
        // dd($test);die;
        $users=User::where('id',$user_id)->get();
        //  dd($users->toArray());
        return view('user.buy.index',compact('values','users'));

    }


    public function indexadmin()
    {
        $values=buy::latest()->paginate(5);
       // dd($values->toArray()); die;
        $user_id=$values[0]['user_id'];
       // dd($test);die;
        $users=User::where('id',$user_id)->get();
      //  dd($users->toArray());
        return view('Admin.buy.index',compact('values','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


        $price=ManagingCurrency::get();
     //  dd($price[0]);
        $cards=\App\account_information::where('user_id',auth()->user()->id)->get();
        $i=0;
        return view('user.buy.currency_buy' ,compact('cards' ,'i' , 'price'));
    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BuyRequest $request)
    {
       //    dd($request->all());

        $buy=$request->all();
        $buy['user_id']=auth()->user()->id;
        $buy['total_price']='22';
        $buy['order_type']='0';
        $buy['state']='0';

        if ( $request->hasFile('imgbank') ) {
            $FileName = time().'.'.$request->file('imgbank')->getClientOriginalExtension();
            if ( $request->file('imgbank')->move( 'assets/imgbank',$FileName ) ) {

                //  dd($FileName);

                $buy['imgbank']=$FileName;
                $buy['sendfishbank']=' ارسال شده است';


                $buy = buy::create($buy);
                alert()->success('', ' عملیات خرید انجام شد ')->persistent('Close');
                return back();
            }



        }





       // alert()->basic('Basic Message', 'Mandatory Title')->autoclose(3500);



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\buy  $buy
     * @return \Illuminate\Http\Response
     */
    public function show(buy $buy)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\buy  $buy
     * @return \Illuminate\Http\Response
     */
    public function edit(buy $buy)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\buy  $buy
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, buy $buy)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\buy  $buy
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $buy1=\App\buy::find($id);
         // dd($buy1->toArray());
        $buy1->delete();

        return back();

    }

    public function destroy2($id)
    {
        //dd($id);
        $buy=buy::find($id);
        $buy->delete();
        return back();

    }
    public function transaction(buy $buy)
    {

        return view('Admin.report.transaction');
    }

    public function transaction2(buy $buy)
    {

        return view('user.report.transaction');
    }

    public function order(buy $buy)
    {
        return view('Admin.report.order');
    }
    public function order2(buy $buy)
    {
        return view('user.report.order');
    }


 /*   public function payment()
    {

        $Amount = 1000; //Amount will be based on Toman - Required
        $Description = 'توضیحات تراکنش تستی'; // Required
        $Email = auth()->user()->email; // Optional
        $Mobile = '09123456789'; // Optional
        $CallbackURL = 'http://localhost:8000/user/panel/sale/checker'; // Required


        $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);

        $result = $client->PaymentRequest(
            [

                'MerchantID' => $this->MerchantID,
                'Amount' => $Amount,
                'Description' => $Description,
                'Email' => $Email,
                'Mobile' => $Mobile,
                'CallbackURL' => $CallbackURL,
            ]
        );

        if ($result->Status == 100) {

            auth()->user()->buy()->create([
                'resnumber'=> $result->Authority,
                'currency_required'=> 1,
                'currency_number'=>2,
                'accountـreceived'=>3,
                'total_price'=>4,
                'orderـtype'=>'5',
                'state'=>'6',

            ]);

            return redirect('https://www.zarinpal.com/pg/StartPay/'.$result->Authority);
        } else {
            echo'ERR: '.$result->Status;
        }
    }
    public function checker()
    {
        $Authority=request('Authority');
        $payment=sale::whereResnumber($Authority)->firstOrFaild();
        if (request('Status') == 'OK') {

            $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);

            $result = $client->PaymentVerification(
                [
                    'MerchantID' => $this->MerchantID,
                    'Authority' => $Authority,
                    'Amount' => $payment->price,
                ]
            );
            if ($result->Status == 100) {
                echo 'Transaction success. RefID:'.$result->RefID;
            } else {
                echo 'Transaction failed. Status:'.$result->Status;
            }
        } else {
            echo 'Transaction canceled by user';


        }
    }*/
    public function verify($id)
    {
        //  dd($id);
        $buy=buy::find($id);
        $buy->update([
            'state'=>'1'
        ]);
        return back();

    }
}
