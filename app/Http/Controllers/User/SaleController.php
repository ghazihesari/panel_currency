<?php

namespace App\Http\Controllers\user;

use App\Bank;
use App\buy;
use App\Http\Controllers\Controller;
use App\Http\Requests\SaleRequest;
use App\ManagingCurrency;
use App\sale;
use App\User;
use Illuminate\Http\Request;
use SoapClient;

class SaleController extends Controller
{
    protected $MerchantID = 'f83cc956-f59f-11e6-889a-005056a205be';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=auth()->user()->id;
        $values=sale::where('user_id',$user)->latest()->paginate(5);
        //  dd($values->toArray()); die;
        $user_id=$values[0]['user_id'];
        // dd($values);die;
        $users=User::where('id',$user_id)->get();
        return view('user.sale.index' ,compact('values','users'));
    }
    public function indexadmin()
    {
        $values=sale::latest()->paginate(5);
        //  dd($values->toArray()); die;
        $user_id=$values[0]['user_id'];
        // dd($values);die;
        $users=User::where('id',$user_id)->get();
        return view('Admin.sale.index' ,compact('values','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $values=Bank::latest()->get();
        $i=0;
        $price=ManagingCurrency::get();
        return view('user.sale.currency_sale' , compact('price','values','i'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SaleRequest $request)
    {
      //  dd($request->all());
        $sale=$request->all();
        $sale['user_id']=auth()->user()->id;
        $sale['total_price']='22';
        $sale['order_type']='32';
        $sale['state']='0';
        $sale=sale::create($sale);
        alert()->success('', ' عملیات فروش انجام شد ')->persistent('Close');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function show(sale $sale)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function edit(sale $sale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, sale $sale)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\sale  $sale
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)

    {

        $sale1=\App\sale::find($id);
        // dd($buy1->toArray());
        $sale1->delete();

        return back();



    }

    public function destroy2($id)
    {
         //dd($id);
        $sale=sale::find($id);
        $sale->delete();
        return back();

    }

    public function payment()
    {

        $Amount = 1000; //Amount will be based on Toman - Required
        $Description = 'توضیحات تراکنش تستی'; // Required
        $Email = auth()->user()->email; // Optional
        $Mobile = '09123456789'; // Optional
        $CallbackURL = 'http://localhost:8000/user/panel/sale/checker'; // Required


        $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);

        $result = $client->PaymentRequest(
            [

                'MerchantID' => $this->MerchantID,
                'Amount' => $Amount,
                'Description' => $Description,
                'Email' => $Email,
                'Mobile' => $Mobile,
                'CallbackURL' => $CallbackURL,
            ]
        );

        if ($result->Status == 100) {

            auth()->user()->sale()->create([
                'resnumber'=> $result->Authority,
                'submitted_currency'=> 1,
                'price'=>2,
                'currency_account'=>3,
                'total_price'=>4,
                'orderـtype'=>'5',
                'state'=>'6',

            ]);

            return redirect('https://www.zarinpal.com/pg/StartPay/'.$result->Authority);
        } else {
            echo'ERR: '.$result->Status;
        }
    }
    public function checker()
    {
        $Authority=request('Authority');
        $payment=sale::whereResnumber($Authority)->firstOrFaild();
        if (request('Status') == 'OK') {

            $client = new SoapClient('https://www.zarinpal.com/pg/services/WebGate/wsdl', ['encoding' => 'UTF-8']);

            $result = $client->PaymentVerification(
                [
                    'MerchantID' => $this->MerchantID,
                    'Authority' => $Authority,
                    'Amount' => $payment->price,
                ]
            );
            if ($result->Status == 100) {
                echo 'Transaction success. RefID:'.$result->RefID;
            } else {
                echo 'Transaction failed. Status:'.$result->Status;
            }
            } else {
            echo 'Transaction canceled by user';


        }
    }
    public function verify($id)
    {
        //  dd($id);
        $sale=sale::find($id);
        $sale->update([
            'state'=>'1'
        ]);
        return back();

    }
}
