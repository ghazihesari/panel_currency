<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\status_buy_sale;
use Illuminate\Http\Request;

class StatusBuySaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status=status_buy_sale::where('id',1)->get();
       // dd($status);

        return view('Admin.statusbuysale.index' , compact('status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //  dd($request->all());
      /*  $value=$request->all();
        $value=status_buy_sale::create($value);
        return back();*/
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\status_buy_sale  $status_buy_sale
     * @return \Illuminate\Http\Response
     */
    public function show(status_buy_sale $status_buy_sale)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\status_buy_sale  $status_buy_sale
     * @return \Illuminate\Http\Response
     */
    public function edit(status_buy_sale $status_buy_sale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\status_buy_sale  $status_buy_sale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, status_buy_sale $status_buy_sale)
    {
       // dd($request->all());
        $status = status_buy_sale::where('id','1')->first();
       // dd($status->toArray());

        $status->update($request->all());

        alert()->success('', ' عملیات با موفقیت انجام شد ')->persistent('Close');
        return redirect(route('statusbuysale.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\status_buy_sale  $status_buy_sale
     * @return \Illuminate\Http\Response
     */
    public function destroy(status_buy_sale $status_buy_sale)
    {
        //
    }
}
