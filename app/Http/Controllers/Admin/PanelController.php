<?php

namespace App\Http\Controllers\Admin;

use App\account_information;
use App\buy;
use App\sale;
use App\ticket;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PanelController extends Controller

{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $countuser=User::count();
        $countticket=ticket::count();
        $countbuy=buy::count();
        $countsale=sale::count();
      //  dd($countuser);


        $values=buy::latest()->paginate(12);
        // dd($values->toArray()); die;
        $user_id=$values[0]['user_id'];
        // dd($test);die;
        $users=User::where('id',$user_id)->get();
        return view('Admin.panel' , compact('countuser' , 'countticket' , 'values' , 'users' , 'countbuy' , 'countsale'));
    }

    public function index2()
    {
        $user=auth()->user()->id;

        $countticket=ticket::where('user_id',$user)->count();
        $countbuy=buy::where('user_id',$user)->count();
        $countsale=sale::where('user_id',$user)->count();
        $countacc=account_information::where('user_id',$user)->count();
        //  dd($countuser);


        $values=buy::where('user_id',$user)->latest()->paginate(12);
        $values2=sale::where('user_id',$user)->latest()->paginate(12);
        // dd($values->toArray()); die;
        $user_id=$values[0]['user_id'];
        // dd($test);die;
        $users=User::where('id',$user_id)->get();
        return view('user.panel' , compact('countacc' ,'countuser' , 'countticket' , 'values','values2' , 'users' , 'countbuy' , 'countsale'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
