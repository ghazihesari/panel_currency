<?php

namespace App\Http\Controllers\Admin;

use App\generalsetting;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GeneralsettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Admin.GeneralSetting.GeneralSetting');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $value=$request->all();




        if ( $request->hasFile('companylogo') ) {
            $FileName = time() . '.' . $request->file('companylogo')->getClientOriginalExtension();
            if ($request->file('companylogo')->move('assets/companylogo', $FileName)) {

                //  dd($FileName);

                $value['logo'] = $FileName;

            }

        }
       // dd($value);
        $value = generalsetting::create($value);
         // return redirect(route('setting'));
        alert()->success('', ' عملیات با موفقیت انجام شد ')->persistent('Close');
        return back();
    }


        /*$value=$request->all();
        GeneralsettingController::create($value);
        return back();*/


    /**
     * Display the specified resource.
     *
     * @param  \App\generalsetting  $generalsetting
     * @return \Illuminate\Http\Response
     */
    public function show(generalsetting $generalsetting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\generalsetting  $generalsetting
     * @return \Illuminate\Http\Response
     */
    public function edit(generalsetting $generalsetting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\generalsetting  $generalsetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, generalsetting $generalsetting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\generalsetting  $generalsetting
     * @return \Illuminate\Http\Response
     */
    public function destroy(generalsetting $generalsetting)
    {
        //
    }
}
