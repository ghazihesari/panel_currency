<?php

namespace App\Http\Controllers\Admin;

use App\ActivationCode;
use App\admin;
use App\help;


use App\Http\Requests\UserRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return string
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {

        $users = User::latest()->paginate(5);
      //  $roles=['0'=>'کاربر' , '1'=> 'مدیر'];
     //  dd($users->toArray());
   return view('Admin.users.index', compact('users'));

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return string
     */
    public function create()
    {

       return view('Admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {

        $user=$request->all();

        $user['password'] = bcrypt( $request->password );

        //dd($user);
        $user = User::create($user);
        alert()->success('', ' عملیات با موفقیت انجام شد ')->persistent('Close');
        return redirect(route('users.create'));


       //     return redirect('admin/users');


     //   $value=$request->all();
       //    dd($value);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\help  $help
     * @return \Illuminate\Http\Response
     */
    public function show(help $help)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\help  $help
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //dd($user);
       // $user = User::get();
        return view('Admin.users.edit' , compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\help  $help
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {

        $user->update($request->all());


        return redirect(route('users.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\help $help
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(User $user)
    {

     //   die();
        $user->delete();
      //  $help->delete();
        return back();
    }

    public function athenticationindex2()
    {

        $users = User::latest()->paginate(5);
        //  dd($users->toArray());
        return view('Admin.users.index2', compact('users'));

    }
    public function usersecreport()
    {

        $users=User::where('level','0')->latest()->paginate(5);
        return view('Admin.users.usersecreport',compact('users'));

    }
    public function modirsecreport()
    {
        $users=User::where('level','1')->latest()->paginate(5);
        return view('Admin.users.modirsecreport' , compact('users'));

    }
    public function profile()
    {
        $user_id = auth()->user()->id;

        $value=User::where('id',$user_id)->get();

       // dd($value[0]);
        //$test='1';
        return view('user.user_profile.profile' , compact('value'));

    }
    public function updateprofile(Request $request , User $user)
    {

        $value=$request->all();
        $test=User::where('id',$value['id'])->get();

       $test[0]->update($value);

        // alert()->success('عملیات مورد نظر با موفقیت انجام شد','تایید شد');
        return back();


    }

    public function senddocuments()
    {
        $user_id = auth()->user()->id;

        $value=User::where('id',$user_id)->get();

        return view('user.user_profile.senddocuments' , compact('value'));

    }


    public function updatesenddocuments(Request $request, User $user)
    {
        $user_id = auth()->user()->id;

        $value=User::where('id',$user_id)->get();


        if ( $request->hasFile('imguser') ) {

            $FileNameimguser ='imguser'. time() . '.' . $request->file('imguser')->getClientOriginalExtension();

            if ($request->file('imguser')->move('assets/imguser', $FileNameimguser)) {

              //  dd($FileNameimguser);
                $value[0]['imguser']=$FileNameimguser;

            }

        }
        if ( $request->hasFile('imgnationalcard') ) {

            $FileNameimgnationalcard = 'imgnationalcard' . time() . '.' . $request->file('imgnationalcard')->getClientOriginalExtension();

            if ($request->file('imgnationalcard')->move('assets/imgnationalcard', $FileNameimgnationalcard)) {

               // dd($FileNameimgnationalcard);
                $value[0]['imgnationalcard']=$FileNameimgnationalcard;
                $value[0]['scan_national_code']='4';



            }
        }
        if ( $request->hasFile('imgidcard') ) {

            $FileNameimgidcard = 'imgidcard' . time() . '.' . $request->file('imgidcard')->getClientOriginalExtension();

            if ($request->file('imgidcard')->move('assets/imgidcard', $FileNameimgidcard)) {

             //   dd($FileNameimgidcard);
                $value[0]['imgidcard']=$FileNameimgidcard;
                $value[0]['scan_birth_certificate']='4';


            }
        }
        if ( $request->hasFile('imgbill') ) {

            $FileNameimgbill = 'imgbill' . time() . '.' . $request->file('imgbill')->getClientOriginalExtension();

            if ($request->file('imgbill')->move('assets/imgidcard', $FileNameimgbill)) {

              //  dd($FileNameimgbill);
                $value[0]['imgbill']=$FileNameimgbill;
                $value[0]['scane_bill']='4';



            }
        }
        //dd($value[0]);
        $value[0]->update(
            [
                'imguser' =>	 $FileNameimguser ,
                'imgnationalcard' => $FileNameimgnationalcard ,
                'imgidcard'=> $FileNameimgidcard ,
                'imgbill'=> $FileNameimgbill,
            ]);
        return back();

    }
    public function resetpassword()
    {

        return view('user.user_profile.resetpassword');

    }
    public function verify($id)
    {
        //  dd($id);
        $admin=User::find($id);
        $admin->update([
            'level'=>'1'
        ]);
        $value = admin::create([
            'user_id'=>$id
        ]);

        return back();

    }
    public function verify2($id)
    {
        //  dd($id);
        $value=User::find($id);
        $value->update([

            'scan_birth_certificate'=>'3',
            'scan_national_code'=>'3',
            'scane_bill'=>'3',
        ]);
        return back();

    }
    public function activation($token)
    {
        $activationcode=ActivationCode::whereCode($token)->first();

        if(! $activationcode)
        {
            dd('not exist');
            return redirect('/');
        }

        if($activationcode->expire < Carbon::now()){
            dd('expire');
            return redirect('/');
        }

        if($activationcode->used == 'true'){
            dd('used');
            return redirect('/');
        }

        $activationcode->user()->update([
            'active'=> 1
        ]);

        $activationcode->update([

            'used'=> true
        ]);

        auth()->loginUsingId($activationcode->user->id);
        return redirect('/');

    }
    public function updatepass(Request $request )
    {
       // $user=$request->all();
        $user_id=auth()->user()->id;
        $user=User::where('id',$user_id)->first();
       // dd($value);
        if($request->pass1 == $request->pass2)
        {
         //   $user['password'] = bcrypt( $request->pass1 );
            $user->update(
                [
                    'password' =>bcrypt( $request->pass1 )
                ]);
            //dd($user->toArray());
        }
        alert()->success('', ' پسورد با موفقیت تغییر یافت ')->persistent('Close');

        return back();

    }

}
