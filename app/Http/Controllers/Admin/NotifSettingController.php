<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\notif_setting;
use Illuminate\Http\Request;

class NotifSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $status=notif_setting::where('id',1)->get();
      //  dd($status);
        return view('Admin.notifsetting.index' , compact('status'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\notif_setting  $notif_setting
     * @return \Illuminate\Http\Response
     */
    public function show(notif_setting $notif_setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\notif_setting  $notif_setting
     * @return \Illuminate\Http\Response
     */
    public function edit(notif_setting $notif_setting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\notif_setting  $notif_setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, notif_setting $notif_setting)
    {
        //dd($request->all());
        $value = notif_setting::where('id','1')->get();
        //$value=$request->all();
       // dd($value[0]);
        $value[0]->update($request->all());
        alert()->success('', ' عملیات با موفقیت انجام شد ')->persistent('Close');
        return redirect('/admin/panel');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\notif_setting  $notif_setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(notif_setting $notif_setting)
    {
        //
    }
}
