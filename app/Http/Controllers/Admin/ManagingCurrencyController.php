<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\ManagingCurrency;
use Illuminate\Http\Request;

class ManagingCurrencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $managingCurrency=ManagingCurrency::where('id',1)->get();

        return view('Admin.managingcurrency.managingcurrency' , compact('managingCurrency'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ManagingCurrency  $managingCurrency
     * @return \Illuminate\Http\Response
     */
    public function show(ManagingCurrency $managingCurrency)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ManagingCurrency  $managingCurrency
     * @return \Illuminate\Http\Response
     */
    public function edit(ManagingCurrency $managingCurrency)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ManagingCurrency  $managingCurrency
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ManagingCurrency $managingCurrency)
    {
       // die('321');

        $managingCurrency = ManagingCurrency::where('id',1)->get();
       // dd($managingCurrency[0]);
        $managingCurrency[0]->update($request->all());
        alert()->success('', ' عملیات با موفقیت انجام شد ')->persistent('Close');

        return redirect(route('managingcurrency.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ManagingCurrency  $managingCurrency
     * @return \Illuminate\Http\Response
     */
    public function destroy(ManagingCurrency $managingCurrency)
    {
        //
    }
}
