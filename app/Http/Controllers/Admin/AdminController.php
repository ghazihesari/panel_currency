<?php

namespace App\Http\Controllers\Admin;

use App\admin;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminRequest;
use App\Http\Requests\UserRequest;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins=User::where('level','1')->latest()->paginate(5);

        return view('Admin.modir.index', compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Admin.modir.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AdminRequest $request)
    {

        $user = new User($request->all());
        $user['password'] = bcrypt( $request->password );
        $user['level'] = '1';
        $user->save();
        $modir = new admin($request->all());
        $modir->user_id = $user->id;
        $modir->save();
        alert()->success('', ' عملیات با موفقیت انجام شد ')->persistent('Close');
        return redirect(route('modir.create'));



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function show(admin $admin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       // dd($id);

     //   dd($admin->toArray());
       // $users = User::with('admin')->where('id','$amdin->user_id');

       // dd($users);
      //  return view('Admin.modir.edit', compact('users'));
      // return response()->json($users);
        $user=User::where('id',$id)->first();
       // $user.=admin::where('user_id',$id)->first();

      /*  $user=DB::table('admins')->join('users','users.id','=','admins.user_id')
          //  ->where('users.id','=',$id)->where('admins.user_id' , '=', $id)
            ->first();*/
        /*$user=DB::table('users')->join('admins','admins.user_id','=','users.id')
             ->where('users.id','=',$id)->where('admins.user_id' , '=', $id)
            ->first();*/


        return view('Admin.modir.edit', compact('user'));




    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\admin  $admin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $user=User::find($id);
        $admin=admin::where('user_id' , $user->id)->first();
        //dd($request->all());
       $user->update($request->all());
       $admin->update($request->all());
       return back();


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\admin $admin
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $user=User::find($id);
        $admin=admin::where('user_id' , $user->id)->first();
        //dd($admin->user_id);
        $user->delete();
        $admin->delete();
        return back();

    }

}
