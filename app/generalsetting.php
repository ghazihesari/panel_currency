<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class generalsetting extends Model
{

    protected $fillable = [
        'sitename','companyname','supportemail','adminemail',
        'tell','phone','addresscompany','countbuy_day','intercambios_day',
        'logo',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */



}
