<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class admin extends Model
{
    protected $fillable = [
        'id','user_id','create_user','edit_user','create_admin',
        'edit_admin','verify_users','ticket_support',
        'notifications','security_reports','list_of_transactions',
        'orders_list','manage_orders','currency_accounts',
        'currency_trading_status','currency','rial_accounts',
        'internal_ports','system_settings',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
