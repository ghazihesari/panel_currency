<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id','level','name','sex','father_name','date_of_birth' ,
        'id_birth_certificate','national_code',
        'mobile','phone','province','city','address','postal_code',
        'max_daily_transaction','max_daily_purchase_amount',
        'scan_birth_certificate','scan_national_code','scane_bill',
        'phone_confirmation','mobile_confirmation','user_status',
        'email','email_verified_at', 'password',
        'remember_token','ip','device_name','os','browser','created_at','updated_at'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];




    public function admin()
    {
        return $this->hasMany(admin::class);
    }
    public function sale()
    {
        return $this->hasMany(sale::class);
    }
    public function buy()
    {
        return $this->hasMany(buy::class);
    }
    public function voucher()
    {
        return $this->hasMany(voucher::class);
    }
    public function account_information()
    {
        return $this->hasMany(account_information::class);
    }


}
