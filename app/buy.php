<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class buy extends Model
{
    //


    protected $fillable = [
        'id','user_id','currency_required','currency_number',
        'account_received','total_price','order_type',
        'state','imgbank','sendfishbank'

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
