<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class account_information extends Model
{
    //
    protected $fillable = [
        'user_id','account_number','bank_name',
        'card_number','shaba_number','scan_card','status_scan_card',
        'state',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
