<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class sale extends Model
{
    //

    protected $fillable = [
        'user_id','submitted_currency','price',
        'currency_account','total_price','order_type',
        'state',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
