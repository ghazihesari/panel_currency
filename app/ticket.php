<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ticket extends Model
{
    //

    protected $fillable = [
        'id','user_id','title','support_unit',
        'priority','description','attached',
        'state',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
