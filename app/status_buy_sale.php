<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class status_buy_sale extends Model
{
    protected $fillable = [
        'id',
        'sale_perfectmani','sale_webmani','sale_paypal','sale_bitcoin',
        'sale_payza','sale_okpay','sale_egopay','sale_estrippay',
        'sale_vax','sale_dolar','sale_yoro','sale_lir','sale_pond','sale_rial',
        'buy_perfectmani','buy_webmani','buy_paypal','buy_bitcoin',
        'buy_payza','buy_okpay','buy_egopay','buy_estrippay',
        'buy_vax','buy_dolar','buy_yoro','buy_lir','buy_pond','buy_rial',


    ];
}
