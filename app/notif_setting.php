<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class notif_setting extends Model
{
    protected $fillable = [
        'id',
        'sendemail_join_the_user','sendemail_verification_of_douc_the_user','sendemail_user_confirmation_user',
        'sendemail_verify_bank_account_user','sendemail_transaction_user','sendemail_order_confirmation_user',
        'sendemail_change_tickerstatus_user','sendemail_subscribe_admin','sendemail_verification_documents_manager',
        'sendemail_user_confirmation_admin','sendemail_confirming_bank','sendemail_transaction_manager',
        'sendemail_order_confirmation_manager','sendemail_change_tickerstatus_manager',

        'sendsms_join_the_user','sendsms_verification_of_douc_the_user','sendsms_user_confirmation_user',
        'sendsms_verify_bank_account_user','sendsmsemaila_transaction_user','sendsms_order_confirmation_user',
        'sendsms_change_tickerstatus_user','sendsms_subscribe_admin','sendsms_verification_documents_manager',
        'sendsms_user_confirmation_admin','sendsms_confirming_bank','sendsms_transaction_manager',
        'sendsms_order_confirmation_manager','sendsms_change_tickerstatus_manager',




    ];
}
