<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('level')->default('0');
            $table->boolean('active')->default('0');
            $table->string('name');
            $table->string('sex')->nullable();
            $table->string('father_name')->nullable();
            $table->string('date_of_birth')->nullable();
            $table->string('id_birth_certificate')->nullable();
            $table->string('national_code')->nullable();
            $table->string('mobile')->nullable();
            $table->string('phone')->nullable();
            $table->string('province')->nullable();
            $table->string('city')->nullable();
            $table->text('address')->nullable();
            $table->string('postal_code')->nullable();
            $table->integer('max_daily_transaction')->nullable();
            $table->integer('max_daily_purchase_amount')->nullable();
            $table->string('scan_birth_certificate')->default('2');
            $table->string('scan_national_code')->default('2');
            $table->string('scane_bill')->default('2');
            $table->string('phone_confirmation')->nullable();
            $table->string('mobile_confirmation')->nullable();
            $table->string('user_status')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('ip')->default('0');
            $table->string('device_name')->default('نامعلوم');
            $table->string('os')->default('نامعلوم');
            $table->string('browser')->default('نامعلوم');
            $table->string('imguser')->nullable();
            $table->string('imgnationalcard')->nullable();
            $table->string('imgidcard')->nullable();
            $table->string('imgbill')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
