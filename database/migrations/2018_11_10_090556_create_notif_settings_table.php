<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotifSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notif_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('sendemail_join_the_user');
            $table->boolean('sendemail_verification_of_douc_the_user');
            $table->boolean('sendemail_user_confirmation_user');
            $table->boolean('sendemail_verify_bankaccount_user');
            $table->boolean('sendemail_transaction_user');
            $table->boolean('sendemail_order_confirmation_user');
            $table->boolean('sendemail_change_tickerstatus_user');
            $table->boolean('sendemail_subscribe_admin');
            $table->boolean('sendemail_verification_documents_manager');
            $table->boolean('sendemail_user_confirmation_admin');
            $table->boolean('sendemail_confirming_bank_account_manager');
            $table->boolean('sendemail_transaction_manager');
            $table->boolean('sendemail_order_confirmation_manager');
            $table->boolean('sendmail_change_tickerstatus_manager');
            $table->boolean('sendsms_join_the_user');
            $table->boolean('sendsms_verification_of_douc_the_user');
            $table->boolean('sendsms_user_confirmation_user');
            $table->boolean('sendsms_verify_bankaccount_user');
            $table->boolean('sendsms_transaction_user');
            $table->boolean('sendsms_order_confirmation_user');
            $table->boolean('sendsms_change_tickerstatus_user');
            $table->boolean('sendsms_subscribe_admin');
            $table->boolean('sendsms_verification_documents_manager');
            $table->boolean('sendsms_user_confirmation_admin');
            $table->boolean('sendsms_confirming_bank_account_manager');
            $table->boolean('sendsms_transaction_manager');
            $table->boolean('sendsms_order_confirmation_manager');
            $table->boolean('sendsms_change_tickerstatus_manager');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notif_settings');
    }
}
