<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralsettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generalsettings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sitename')->nullable();
            $table->string('companyname')->nullable();
            $table->string('supportemail')->nullable();
            $table->string('adminemail')->nullable();
            $table->string('tell')->nullable();
            $table->string('phone')->nullable();
            $table->string('addresscompany')->nullable();
            $table->integer('countbuy_day')->nullable();
            $table->string('intercambios_day')->nullable();
            $table->string('logo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generalsettings');
    }
}
