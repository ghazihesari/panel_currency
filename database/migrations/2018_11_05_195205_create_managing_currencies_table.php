<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateManagingCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('managing_currencies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchaseprice_perfectmani')->default(0);
            $table->integer('purchaseprice_webmani')->default(0);
            $table->integer('purchaseprice_bitcoin')->default(0);
            $table->integer('purchaseprice_paypal')->default(0);
            $table->integer('purchaseprice_vax')->default(0);
            $table->integer('purchaseprice_egopay')->default(0);
            $table->integer('purchaseprice_okpay')->default(0);
            $table->integer('purchaseprice_payza')->default(0);
            $table->integer('purchaseprice_estrippay')->default(0);
            $table->integer('purchaseprice_dolar')->default(0);
            $table->integer('purchaseprice_yoro')->default(0);
            $table->integer('purchaseprice_lir')->default(0);
            $table->integer('purchaseprice_pond')->default(0);
            $table->integer('purchaseprice_rial')->default(0);
            $table->integer('salesprice_perfectmani')->default(0);
            $table->integer('salesprice_webmani')->default(0);
            $table->integer('salesprice_bitcoin')->default(0);
            $table->integer('salesprice_paypal')->default(0);
            $table->integer('salesprice_vax')->default(0);
            $table->integer('salesprice_egopay')->default(0);
            $table->integer('salesprice_okpay')->default(0);
            $table->integer('salesprice_payza')->default(0);
            $table->integer('salesprice_estrippay')->default(0);
            $table->integer('salesprice_dolar')->default(0);
            $table->integer('salesprice_yoro')->default(0);
            $table->integer('salesprice_lir')->default(0);
            $table->integer('salesprice_pond')->default(0);
            $table->integer('salesprice_rial')->default(0);
            $table->integer('count_perfectmani')->default(0);
            $table->integer('count_webmani')->default(0);
            $table->integer('count_bitcoin')->default(0);
            $table->integer('count_paypal')->default(0);
            $table->integer('count_vax')->default(0);
            $table->integer('count_egopay')->default(0);
            $table->integer('count_okpay')->default(0);
            $table->integer('count_payza')->default(0);
            $table->integer('count_estrippay')->default(0);
            $table->integer('count_dolar')->default(0);
            $table->integer('count_yoro')->default(0);
            $table->integer('count_lir')->default(0);
            $table->integer('count_pond')->default(0);
            $table->integer('count_rial')->default(0);
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('managing_currencies');
    }
}
