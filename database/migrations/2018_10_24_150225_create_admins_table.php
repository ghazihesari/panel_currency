<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->boolean('create_user')->default('0');
            $table->boolean('edit_user')->default('0');
            $table->boolean('create_admin')->default('0');
            $table->boolean('edit_admin')->default('0');
            $table->boolean('verify_users')->default('0');
            $table->boolean('ticket_support')->default('0');
            $table->boolean('notifications')->default('0');
            $table->boolean('security_reports')->default('0');
            $table->boolean('list_of_transactions')->default('0');
            $table->boolean('orders_list')->default('0');
            $table->boolean('manage_orders')->default('0');
            $table->boolean('currency_accounts')->default('0');
            $table->boolean('currency_trading_status')->default('0');
            $table->boolean('currency')->default('0');
            $table->boolean('rial_accounts')->default('0');
            $table->boolean('internal_ports')->default('0');
            $table->boolean('system_settings')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
