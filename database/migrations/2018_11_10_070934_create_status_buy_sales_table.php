<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatusBuySalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('status_buy_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('sale_perfectmani')->default('1');
            $table->boolean('sale_webmani')->default('1');
            $table->boolean('sale_paypal')->default('1');
            $table->boolean('sale_bitcoin')->default('1');
            $table->boolean('sale_payza')->default('1');
            $table->boolean('sale_okpay')->default('1');
            $table->boolean('sale_egopay')->default('1');
            $table->boolean('sale_estrippay')->default('1');
            $table->boolean('sale_dolar')->default('1');
            $table->boolean('sale_yoro')->default('1');
            $table->boolean('sale_lir')->default('1');
            $table->boolean('sale_pond')->default('1');
            $table->boolean('sale_rial')->default('1');

            $table->boolean('buy_perfectmani')->default('1');
            $table->boolean('buy_webmani')->default('1');
            $table->boolean('buy_paypal')->default('1');
            $table->boolean('buy_bitcoin')->default('1');
            $table->boolean('buy_payza')->default('1');
            $table->boolean('buy_okpay')->default('1');
            $table->boolean('buy_egopay')->default('1');
            $table->boolean('buy_estrippay')->default('1');
            $table->boolean('buy_vax')->default('1');
            $table->boolean('buy_dolar')->default('1');
            $table->boolean('buy_yoro')->default('1');
            $table->boolean('buy_lir')->default('1');
            $table->boolean('buy_pond')->default('1');
            $table->boolean('buy_rial')->default('1');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('status_buy_sales');
    }
}
