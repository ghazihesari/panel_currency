<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/admin/panel');
});

Auth::routes();



Route::get('/user/active/email/{token}','Admin\UserController@activation')->name('activation.account');


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin/panel/athen', 'Admin\UserController@athenticationindex2')->name('users.athen');
Route::get('/admin/panel/usersecreport', 'Admin\UserController@usersecreport')->name('users.usersecreport');
Route::get('/admin/panel/modirsecreport', 'Admin\UserController@modirsecreport')->name('users.modirsecreport');
Route::get('/admin/panel/transaction', 'user\BuyController@transaction');
Route::get('/admin/panel/order', 'user\BuyController@order');
$this->get('/admin/panel/ticket', 'user\TicketController@index2');
$this->get('/admin/panel/sale', 'user\SaleController@indexadmin');
$this->get('/admin/panel/buy', 'user\BuyController@indexadmin');
$this->get('/admin/panel/voucher', 'user\VoucherController@indexadmin');
Route::delete('admin/panel/adminticket/{id}', 'user\TicketController@destroy2')->name('adminticket.delete');
Route::post('admin/panel/verify/{id}', 'user\TicketController@verify')->name('adminticket.verify');
Route::post('admin/panel/buyverify/{id}', 'user\BuyController@verify')->name('buy.verify');
Route::post('admin/panel/saleverify/{id}', 'user\SaleController@verify')->name('sale.verify');
Route::post('admin/panel/userverify/{id}', 'Admin\UserController@verify')->name('user.verify');
Route::post('admin/panel/userverify2/{id}', 'Admin\UserController@verify2')->name('user.verify2');
Route::get('/admin/panel/account', 'user\AccountInformationController@indexadmin');
Route::delete('/admin/panel/delaccount/{id}', 'user\AccountInformationController@destroy2')->name('delaccount');
Route::post('/admin/panel/verifyaccount/{id}', 'user\AccountInformationController@verifyaccount')->name('verifyaccount');
Route::delete('/admin/panel/adminsale/{id}', 'user\SaleController@destroy2')->name('adminsale.delete');
Route::delete('/admin/panel/adminbuy/{id}', 'user\BuyController@destroy2')->name('adminbuy.delete');
Route::post('/admin/panel/updatepass', 'Admin\UserController@updatepass')->name('users.updatepass');





Route::group(['namespace' => 'Admin'   ,  'middleware' => ['auth','checkAdmin'], 'prefix' => 'admin'],function () {
    $this->get('/panel', 'PanelController@index');
    $this->resource('/panel/users', 'UserController');
    $this->resource('/panel/modir', 'AdminController');
    $this->resource('/panel/setting', 'GeneralsettingController');
    $this->resource('/panel/managingcurrency' , 'ManagingCurrencyController');
    $this->resource('/panel/statusbuysale' , 'StatusBuySaleController');
    $this->resource('/panel/notifsetting' , 'NotifSettingController');
    $this::resource('/panel/bank' , 'BankController');
    });




$this->get('/user/panel', 'Admin\PanelController@index2');
Route::get('/user/panel/transaction', 'user\BuyController@transaction2');
Route::get('/user/panel/profile', 'Admin\UserController@profile');
Route::patch('/user/panel/profile', 'Admin\UserController@updateprofile')->name('profile.update');
Route::get('/user/panel/senddocuments', 'Admin\UserController@senddocuments');
Route::patch('/user/panel/senddocuments', 'Admin\UserController@updatesenddocuments')->name('senddocuments.update');;
Route::patch('/user/panel/updatesenddocuments', 'Admin\UserController@updatesenddocuments');
Route::get('/user/panel/resetpassword', 'Admin\UserController@resetpassword');
Route::post('/user/panel/salepayment', 'User\SaleController@payment')->name('salepayment');
Route::get('/user/panel/payment/checker', 'User\SaleController@checker');
Route::post('/user/panel/buypayment', 'User\BuyController@payment')->name('buypayment');
Route::get('/user/panel/payment/checker', 'User\BuyController@checker');
Route::post('/user/panel/voucherpayment', 'User\VoucherController@payment')->name('voucherpayment');
Route::get('/user/panel/payment/voucherpayment', 'User\VoucherController@checker');


Route::group(['namespace' => 'User'  , 'prefix' => 'user'],function () {

    $this->resource('/panel/userpanel', 'UserpanelController');
    $this->resource('/panel/ticket', 'TicketController');
    $this->resource('/panel/accountinformation', 'AccountInformationController');
    $this->resource('/panel/sale', 'SaleController');
    $this->resource('/panel/buy', 'BuyController');
    $this->resource('/panel/voucher', 'VoucherController');
});






Route::group(['namespace' => 'Auth'] , function (){
    // Authentication Routes...
    $this->get('login', 'LoginController@showLoginForm')->name('login');
    $this->post('login', 'LoginController@login');
    $this->get('logout', 'LoginController@logout')->name('logout');

    // Login And Register With Google
    // $this->get('login/google', 'LoginController@redirectToProvider');
    // $this->get('login/google/callback', 'LoginController@handleProviderCallback');
    // Registration Routes...
    $this->get('register', 'RegisterController@showRegistrationForm')->name('register');
    $this->post('register', 'RegisterController@register');

    // Password Reset Routes...
    $this->get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('password.request');
    $this->post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    $this->get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('password.reset');
    $this->post('password/reset', 'ResetPasswordController@reset');
});